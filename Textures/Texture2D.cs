﻿using System;
using System.Drawing;
using OpenTK.Graphics.OpenGL;
using Rug.Cmd;

namespace Rug.LiteGL
{
    public class Texture2D
	{
		private string m_Name;

		private Texture2DInfo m_ResourceInfo; 

		#region Public Properties

		public string Name { get { return m_Name; } }

		public Texture2DInfo ResourceInfo { get { return m_ResourceInfo; } }
       
        public uint ResourceHandle { get; protected set; }

        public bool IsLoaded { get; protected set; }

		public TextureTarget TextureTarget { get { return m_ResourceInfo.TextureTarget; } }		

		#endregion

		public Texture2D(string name, Texture2DInfo resourceInfo)
		{
			m_Name = name; 
			m_ResourceInfo = resourceInfo; 
		}

		public override string ToString()
		{
			return String.Format("{0} ({1})", m_Name, ResourceInfo.ToString());
		}

		public virtual void LoadResources()
		{
			if (ResourceHandle != 0 || IsLoaded == true)
			{
				throw new Exception("Attempt to load Texture2D resource '" + Name + "', the resource is already loaded.");
			}

            uint resourceHandle; 
			GL.GenTextures(1, out resourceHandle);
            ResourceHandle = resourceHandle; 

			if (ResourceHandle == 0)
			{
				RC.WriteError(001, "Texture '" + this.Name + "' is not valid");
			}

			GLHelper.CheckTexture();
			GL.BindTexture(TextureTarget, ResourceHandle);
			
            GLHelper.CheckTexture();

			// DO checking 

			IsLoaded = true; 
		}

		public void CreateBlank()
		{
			GL.BindTexture(TextureTarget, ResourceHandle);

			if (ResourceHandle == 0)
			{
				RC.WriteError(001, "Texture '" + this.Name + "' is not valid");
			}

			m_ResourceInfo.OnLoad();

			GL.BindTexture(TextureTarget, 0);
		}

		public void UploadImage(IntPtr intPtr)
		{
			GL.BindTexture(TextureTarget, ResourceHandle);

			if (ResourceHandle == 0)
			{
				RC.WriteError(001, "Texture '" + this.Name + "' is not valid");
			}

			m_ResourceInfo.OnLoad(intPtr);

			GL.BindTexture(TextureTarget, 0);
		}

        public void UploadImage(IntPtr intPtr, ref Rectangle rect)
        {
            GL.BindTexture(TextureTarget, ResourceHandle);

            if (ResourceHandle == 0)
            {
                RC.WriteError(001, "Texture '" + this.Name + "' is not valid");
            }

            m_ResourceInfo.OnLoad(intPtr, ref rect);

            GL.BindTexture(TextureTarget, 0);
        }


        public void DownloadImage(IntPtr intPtr)
		{
			GL.BindTexture(TextureTarget, ResourceHandle);

			m_ResourceInfo.OnDownload(intPtr);

			GL.BindTexture(TextureTarget, 0);
		}

		public virtual void UnloadResources()
		{
			if (ResourceHandle == 0 || IsLoaded == false)
			{				
				return; 
			}

			GL.DeleteTexture(ResourceHandle);
			ResourceHandle = 0;
			IsLoaded = false; 
		}

		public void Bind()
		{
			GL.BindTexture(TextureTarget, ResourceHandle);
		}

		public void Unbind()
		{
			GL.BindTexture(TextureTarget, 0);
		}
	}
}
