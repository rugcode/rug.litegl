﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Rug.LiteGL
{
    public class BitmapTexture2D : Texture2D
	{
        public string ImagePath { get; set; } 

        public Bitmap Image { get; set; } 

		public byte[] ImageData { get; set; }

		public BitmapTexture2D(string name, string path, Texture2DInfo resourceInfo)
			: base(name, resourceInfo)
		{
			ImagePath = path; 
		}

        public BitmapTexture2D(string name, Bitmap image, Texture2DInfo resourceInfo)
            : base(name, resourceInfo)
        {
            Image = image;
        }

		public BitmapTexture2D(string name, byte[] imageData, Texture2DInfo resourceInfo)
			: base(name, resourceInfo)
		{
			ImageData = imageData;
		}

		public override void LoadResources()
		{
            if (Image != null)
            {
                LoadBitmap(Image);
            }
			else if (string.IsNullOrEmpty(ImagePath) == false)
			{
				using (Bitmap bitmap = FileHelper.LoadBitmap(ImagePath))
				{
					LoadBitmap(bitmap);
				}
			}
			else
			{
				using (MemoryStream stream = new MemoryStream(ImageData))
				using (Bitmap bitmap = new Bitmap(stream))
				{
					LoadBitmap(bitmap);
				}
			}
		}

		private void LoadBitmap(Bitmap bitmap)
		{
			this.ResourceInfo.Size = new Size(bitmap.Width, bitmap.Height);

			base.LoadResources();

            UploadImage(bitmap);
		}

        public void UploadImage()
        {
            if (Image != null)
            {
                UploadImage(Image);
            }
            else if (String.IsNullOrEmpty(ImagePath) == false)
            {
                using (Bitmap bitmap = FileHelper.LoadBitmap(ImagePath))
                {
                    UploadImage(bitmap);
                }
            }
            else
            {
                using (MemoryStream stream = new MemoryStream(ImageData))
                using (Bitmap bitmap = new Bitmap(stream))
                {
                    UploadImage(bitmap);
                }
            }
        }

        public void UploadImage(ref Rectangle rect)
        {
            if (Image != null)
            {
                UploadImage(Image, ref rect);
            }
            else if (String.IsNullOrEmpty(ImagePath) == false)
            {
                using (Bitmap bitmap = FileHelper.LoadBitmap(ImagePath))
                {
                    UploadImage(bitmap, ref rect);
                }
            }
            else
            {
                using (MemoryStream stream = new MemoryStream(ImageData))
                using (Bitmap bitmap = new Bitmap(stream))
                {
                    UploadImage(bitmap, ref rect);
                }
            }
        }

        private void UploadImage(Bitmap bitmap)
        {
            BitmapData data;

            if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgra)
            {
                data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            }
            else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgr)
            {
                data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            }
            else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Red)
            {
                data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            }
            else
            {
                data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            }

            UploadImage(data.Scan0);

            bitmap.UnlockBits(data);
        }

        private void UploadImage(Bitmap bitmap, ref Rectangle rect)
        { 
            BitmapData data;

            if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgra)
            {
                data = bitmap.LockBits(rect, ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            }
            else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgr)
            {
                data = bitmap.LockBits(rect, ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            }
            else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Red)
            {
                data = bitmap.LockBits(rect, ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            }
            else
            {
                data = bitmap.LockBits(rect, ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            }

            UploadImage(data.Scan0, ref rect);

            bitmap.UnlockBits(data);
        }
    }
}
