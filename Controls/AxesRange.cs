﻿using System;
using System.Globalization;
using System.Xml;
using OpenTK;

namespace Rug.LiteGL.Controls
{
    public class AxesClamp
    {
        public const float MinValue = -10e9f;
        public const float MaxValue = 10e9f;

        public static float Clamp(float value)
        {
            if (float.IsPositiveInfinity(value)) return MaxValue;
            if (float.IsNegativeInfinity(value)) return MinValue;
            if (float.IsNaN(value)) return 0;

            return Math.Min(Math.Max(value, MinValue), MaxValue);
        }
    }

    public struct AxesRange
    {
        public const double RangeMax = 1000000d;
        public const double RangeMin = 0.001d;

        public double XMax;
        public double XMin;
        public double XOffset;

        public double YMax;
        public double YMin;
        public double YOffset;

        public double XCenter
        {
            get { return XMin + (XRange * 0.5); }

            set
            {
                double min = XMin;
                double max = XMax;

                min -= XCenter;
                max -= XCenter;

                min += value;
                max += value;

                XMin = min;
                XMax = max;
            }
        }

        public double XRange
        {
            get { return XMax - XMin; }

            set
            {
                double center = XCenter;

                double halfValue = value * 0.5;

                XMin = center - halfValue;
                XMax = center + halfValue;
            }
        }

        public double XScale { get { return 2d / XRange; } }

        public double YCenter
        {
            get { return YMin + (YRange * 0.5); }

            set
            {
                double min = YMin;
                double max = YMax;

                min -= YCenter;
                max -= YCenter;

                min += value;
                max += value;

                YMin = min;
                YMax = max;
            }
        }

        public double YRange
        {
            get { return YMax - YMin; }

            set
            {
                double center = YCenter;

                double halfValue = value * 0.5;

                YMin = center - halfValue;
                YMax = center + halfValue;
            }
        }

        public double YScale { get { return 2d / YRange; } }

        public AxesRange(double xMin, double yMin, double xMax, double yMax)
        {
            XMax = xMax;
            YMax = yMax;
            XMin = xMin;
            YMin = yMin;

            XOffset = 0;
            YOffset = 0;
        }

        public static void AppendAttributeAndValue(XmlElement element, string name, AxesRange value)
        {
            Helper.AppendAttributeAndValue(element, name, Serialize(value));
        }

        public static AxesRange Deserialize(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            double xMin, yMin, xMax, yMax;

            xMin = double.Parse(pieces[0], CultureInfo.InvariantCulture);
            yMin = double.Parse(pieces[1], CultureInfo.InvariantCulture);
            xMax = double.Parse(pieces[2], CultureInfo.InvariantCulture);
            yMax = double.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new AxesRange(xMin, yMin, xMax, yMax);
        }

        public static AxesRange GetAttributeValue(XmlNode node, string name, AxesRange @default)
        {
            if (node.Attributes[name] == null)
            {
                return @default;
            }

            try
            {
                return Deserialize(node.Attributes[name].Value);
            }
            catch
            {
                return @default;
            }
        }

        public static string Serialize(AxesRange axesRange)
        {
            return String.Format("{0},{1},{2},{3}",
                axesRange.XMin.ToString(CultureInfo.InvariantCulture),
                axesRange.YMin.ToString(CultureInfo.InvariantCulture),
                axesRange.XMax.ToString(CultureInfo.InvariantCulture),
                axesRange.YMax.ToString(CultureInfo.InvariantCulture));
        }

        public void GetMostSignificantTickX(out double minTick, out double tickSize, out int decimalPlaces)
        {
            GetMostSignificantTick(out minTick, out tickSize, out decimalPlaces, XRange, XMin);
        }

        public void GetMostSignificantTickY(out double minTick, out double tickSize, out int decimalPlaces)
        {
            GetMostSignificantTick(out minTick, out tickSize, out decimalPlaces, YRange, YMin);
        }

        private static void GetMostSignificantTick(out double minTick, out double tickSize, out int decimalPlaces, double range, double min)
        {
            range = MathHelper.Clamp(range, AxesRange.RangeMin, AxesRange.RangeMax);

            int exponent = (int)Math.Ceiling(Math.Log(range) / Math.Log(10));

            decimalPlaces = Math.Abs(Math.Min((int)Math.Ceiling(Math.Log(range * 0.01f) / Math.Log(10)), 0));

            double orderOfMagnitude = Math.Pow(10, exponent);

            tickSize = orderOfMagnitude * 0.01f;

            double sign = Math.Sign(min);

            minTick = Math.Ceiling(Math.Abs(min / orderOfMagnitude)) * orderOfMagnitude * sign;

            while (minTick > min)
            {
                minTick -= orderOfMagnitude;
            }
        }
    }
}