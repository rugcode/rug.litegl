﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using Rug.LiteGL.Simple;

namespace Rug.LiteGL.Controls
{
    public class XYGraph : GraphBase
    {       
        public XYGraph()
        {
        }

        public void AddScopeData(params Vector2[] traceData)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            lock (Traces.SyncRoot)
            {
                for (int i = 0, ie = Math.Min(traceData.Length, Traces.Count); i < ie; i++)
                {
                    Traces[i].ScopeLine?.AddDataPoint(traceData[i]);
                }
            }
        }

        public void AddScopeData(int index, Vector2 traceData)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            lock (Traces.SyncRoot)
            {
                Traces[index].ScopeLine?.AddDataPoint(traceData);
            }
        }
        
        protected override void OnPaintGraph()
        {
        }

        protected override void ConfigureTraces()
        {
            lock (Traces.SyncRoot)
            {
                foreach (Trace trace in Traces)
                {
                    if (trace.ScopeLine == null)
                    {
                        continue; 
                    }

                    trace.ScopeLine.AssumeLinearXValues = false;
                    trace.ScopeLine.RescanXForMinMaxOnBlockUpdate = false;
                }
            }
        }
    }
}
