﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Rug.LiteGL.Controls
{
    public enum GraphType
    {
        SystemTime,
        Timestamp,
        XY,
    }

    public class GraphSettings : ICloneable
    {
        public readonly List<Trace> Traces = new List<Trace>();

        public AxesRange AxesRange { get; set; } = new AxesRange() { XMin = -1f, XMax = 1f, YMin = -1f, YMax = 1f };

        public GraphType GraphType { get; set; } = GraphType.SystemTime;

        /// <summary>
        /// Gets or sets the index of the horizontal auto-scale.
        /// </summary>
        /// <value>The index of the trace to auto-scale the horizontal axis. If less than 0 then horizontal auto-scale is disabled; if greater or equal to the number of traces then the horizontal axis will be auto-scaled for all traces.</value>
        public int HorizontalAutoscaleIndex { get; set; } = -1;

        public bool RollMode { get; set; } = false;

        public bool ShowLegend { get; set; } = true;

        public string Title { get; set; } = "No Name";

        /// <summary>
        /// Gets or sets the index of the vertical auto-scale.
        /// </summary>
        /// <value>The index of the trace to auto-scale the vertical axis. If less than 0 then vertical auto-scale is disabled; if greater or equal to the number of traces then the vertical axis will be auto-scaled for all traces.</value>
        public int VerticalAutoscaleIndex { get; set; } = -1;

        public string XAxisLabel { get; set; } = string.Empty;

        public string YAxisLabel { get; set; } = string.Empty;

        public static bool FromFile(string filePath, out GraphSettings config)
        {
            if (FileHelper.FileExists(filePath) == false)
            {
                config = null;

                return false;
            }

            try
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(filePath);

                config = new GraphSettings();

                config.Load(doc.DocumentElement);

                return true;
            }
            catch (Exception ex)
            {
                config = null;

                return false;
            }
        }

        public static void SaveToFile(string filePath, GraphSettings config)
        {
            Helper.EnsurePathExists(filePath);

            XmlDocument doc = new XmlDocument();

            XmlElement element = Helper.CreateElement(doc, "GraphSettings");

            config.Save(element);

            doc.AppendChild(element);

            doc.Save(filePath);
        }

        public GraphSettings Clone()
        {
            GraphSettings newConfig = new GraphSettings()
            {
                AxesRange = AxesRange,
                Title = Title,
                GraphType = GraphType,
                ShowLegend = ShowLegend,
                HorizontalAutoscaleIndex = HorizontalAutoscaleIndex,
                VerticalAutoscaleIndex = VerticalAutoscaleIndex,
                XAxisLabel = XAxisLabel,
                YAxisLabel = YAxisLabel,
                RollMode = RollMode,
            };

            foreach (Trace trace in Traces)
            {
                newConfig.Traces.Add(trace.Clone());
            }

            return newConfig;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public void Load(XmlNode node)
        {
            Title = Helper.GetAttributeValue(node, nameof(Title), string.Empty);

            if (string.IsNullOrEmpty(Title) == true)
            {
                throw new Exception(string.Format("Missing {0} attribute.", nameof(Title)));
            }

            XAxisLabel = Helper.GetAttributeValue(node, nameof(XAxisLabel), string.Empty);
            YAxisLabel = Helper.GetAttributeValue(node, nameof(YAxisLabel), string.Empty);
            GraphType = Helper.GetAttributeValue(node, nameof(GraphType), GraphType.SystemTime);
            RollMode = Helper.GetAttributeValue(node, nameof(RollMode), false);
            AxesRange = AxesRange.GetAttributeValue(node, nameof(AxesRange), AxesRange);
            ShowLegend = Helper.GetAttributeValue(node, nameof(ShowLegend), true);
            HorizontalAutoscaleIndex = Helper.GetAttributeValue(node, nameof(HorizontalAutoscaleIndex), -1);
            VerticalAutoscaleIndex = Helper.GetAttributeValue(node, nameof(VerticalAutoscaleIndex), -1);

            Traces.Clear();
            foreach (XmlNode traceNode in node.ChildNodes)
            {
                if (traceNode.Name != "Trace")
                {
                    continue;
                }

                Trace trace = new Trace();

                trace.Load(traceNode);

                Traces.Add(trace);
            }
        }

        public XmlElement Save(XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Title), Title);
            Helper.AppendAttributeAndValue(element, nameof(XAxisLabel), XAxisLabel);
            Helper.AppendAttributeAndValue(element, nameof(YAxisLabel), YAxisLabel);
            Helper.AppendAttributeAndValue(element, nameof(GraphType), GraphType);
            Helper.AppendAttributeAndValue(element, nameof(RollMode), RollMode);
            Helper.AppendAttributeAndValue(element, nameof(ShowLegend), ShowLegend);
            AxesRange.AppendAttributeAndValue(element, nameof(AxesRange), AxesRange);
            Helper.AppendAttributeAndValue(element, nameof(HorizontalAutoscaleIndex), HorizontalAutoscaleIndex);
            Helper.AppendAttributeAndValue(element, nameof(VerticalAutoscaleIndex), VerticalAutoscaleIndex);

            foreach (Trace trace in Traces)
            {
                XmlElement traceNode = Helper.CreateElement(element, "Trace");

                trace.Save(traceNode);

                element.AppendChild(traceNode);
            }

            return element;
        }
    }
}