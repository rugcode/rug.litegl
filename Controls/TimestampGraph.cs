﻿using System;
using System.ComponentModel;
using OpenTK;
using Rug.LiteGL.Simple;

namespace Rug.LiteGL.Controls
{
    public class TimestampGraph : GraphBase
    {
        private DateTime baseTime;
        private DateTime lastTimestamp;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool Rolling { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool TrackHorizontalTrace { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(-1)]
        public int TrackHorizontalTraceIndex { get; set; } = -1;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool TrackVerticalTrace { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(-1)]
        public int TrackVerticalTraceIndex { get; set; } = -1;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool RescanYForMinMaxOnBlockUpdate { get; set; } = false;


        public TimestampGraph()
        {
            baseTime = DateTime.MinValue;
            lastTimestamp = DateTime.MinValue;
            XAxisLabel = "Seconds (" + Helper.DateTimeToString(baseTime, true) + ")";
        }

        public void AddScopeData(DateTime timestamp, params float[] traceData)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            lock (Traces.SyncRoot)
            {
                float x = CheckForTimeReset(timestamp);

                for (int i = 0, ie = Math.Min(traceData.Length, Traces.Count); i < ie; i++)
                {
                    Traces[i].ScopeLine?.AddDataPoint(new Vector2(x, traceData[i]));
                }
            }
        }

        public void AddScopeData(DateTime timestamp, int index, float traceData)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            lock (Traces.SyncRoot)
            {
                float x = CheckForTimeReset(timestamp);

                Traces[index].ScopeLine?.AddDataPoint(new Vector2(x, traceData));
            }
        }

        public void CenterOnTrace(int index)
        {
            VerticalCenterAndRangeOnTrace(index, false);
        }

        public override void Clear()
        {
            baseTime = lastTimestamp;

            base.Clear();
        }

        protected override void ConfigureTraces()
        {
            lock (Traces.SyncRoot)
            {
                foreach (Trace trace in Traces)
                {
                    if (trace.ScopeLine == null)
                    {
                        continue; 
                    }

                    TraceData line = trace.ScopeLine; 

                    line.RescanYForMinMaxOnBlockUpdate = RescanYForMinMaxOnBlockUpdate;

                    if (Rolling == true)
                    {
                        line.Min.X = line.FirstDataPoint.X;
                        line.Max.X = line.LastDataPoint.X;

                        line.RolloutBuffer = true;
                    }
                    else
                    {
                        if (TrackHorizontalTrace == true)
                        {
                            line.Min.X = 0;
                        }

                        line.AssumeLinearXValues = true;
                        line.RescanXForMinMaxOnBlockUpdate = true;

                        line.RolloutBuffer = TrackHorizontalTrace;
                    }
                }
            }
        }

        protected override void OnPaintGraph()
        {
            AxesRange.XOffset = 0;

            ConfigureTraces();

            if (TrackVerticalTrace == true)
            {
                VerticalCenterAndRangeOnTrace(TrackVerticalTraceIndex, true);
            }

            if (TrackHorizontalTrace == true)
            {
                HorizontalRangeOnTrace(TrackHorizontalTraceIndex);
            }

            if (Rolling == true)
            {
                AxesRange.XCenter = -(AxesRange.XRange * 0.5f); // (float)(lastTimestamp - baseTime).TotalSeconds - (AxesRange.RangeX * 0.5f);
                AxesRange.XOffset = ((float)(lastTimestamp - baseTime).TotalSeconds); // - AxesRange.RangeX;// - (AxesRange.RangeX * 0.5f));

                XAxisLabel = "Seconds (" + Helper.DateTimeToString(lastTimestamp, true) + ")";

                UpdateGridLines();
            }
        }

        private float CheckForTimeReset(DateTime timestamp)
        {
            bool inPast = timestamp.Add(new TimeSpan(0, 0, 0, 0, 100)) < lastTimestamp;

            lastTimestamp = timestamp;

            if (inPast == false && (TrackHorizontalTrace == true || Rolling == true))
            {
                if (baseTime == DateTime.MinValue)
                {
                    baseTime = lastTimestamp;

                    XAxisLabel = "Seconds (" + Helper.DateTimeToString(baseTime, true) + ")";
                }
            }

            float x = (float)(lastTimestamp - baseTime).TotalSeconds;

            if (inPast == false && Rolling == true)
            {
                return x;
            }

            if (inPast == false && TrackHorizontalTrace == true)
            {
                bool oneOrMoreBuffersFull = false;

                lock (Traces.SyncRoot)
                {
                    foreach (Trace trace in Traces)
                    {
                        if (trace.ScopeLine == null)
                        {
                            continue; 
                        }

                        if (trace.ScopeLine.BufferIsFull == true)
                        {
                            oneOrMoreBuffersFull = true;
                        }
                    }
                }

                if (oneOrMoreBuffersFull == false)
                {
                    return x;
                }
            }

            double xScale = AxesRange.XScale;

            if (inPast == true || x > 2f / xScale)
            {
                baseTime = lastTimestamp;

                XAxisLabel = "Seconds (" + Helper.DateTimeToString(baseTime, true) + ")";

                lock (Traces.SyncRoot)
                {
                    foreach (Trace trace in Traces)
                    {
                        trace.ScopeLine?.Clear();
                    }
                }
                x = 0;

                base.UpdateGridLines();
            }

            return x;
        }

        private void HorizontalRangeOnTrace(int horizontalIndex)
        {
            AxesRange initialRange = AxesRange;

            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = new Vector2(float.MinValue, float.MinValue);

            bool gotOne = false;

            lock (Traces.SyncRoot)
            {
                for (int i = 0; i < Traces.Count; i++)
                {
                    if (horizontalIndex < Traces.Count && horizontalIndex != i)
                    {
                        continue;
                    }

                    TraceData scopeLine = Traces[i].ScopeLine;

                    if (scopeLine == null)
                    {
                        continue; 
                    }

                    if (scopeLine.DataPoints < 2)
                    {
                        continue;
                    }

                    gotOne = true;

                    min = Vector2.ComponentMin(min, scopeLine.Min);
                    max = Vector2.ComponentMax(max, scopeLine.Max);
                }
            }

            if (gotOne == false)
            {
                return;
            }

            AxesRange.XCenter = (max.X - min.X) * 0.5f + min.X;

            double range = (max.X - min.X);

            range = MathHelper.Clamp(range, AxesRange.RangeMin, AxesRange.RangeMax);

            AxesRange.XRange = range;

            if (AxesRange.XCenter != initialRange.XCenter ||
                AxesRange.XRange != initialRange.XRange)
            {
                UpdateGridLines();
            }
        }

        private void VerticalCenterAndRangeOnTrace(int verticalIndex, bool doVerticalRange)
        {
            AxesRange initialRange = AxesRange;

            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = new Vector2(float.MinValue, float.MinValue);

            bool gotOne = false;

            lock (Traces.SyncRoot)
            {
                for (int i = 0; i < Traces.Count; i++)
                {
                    if (verticalIndex < Traces.Count && verticalIndex != i)
                    {
                        continue;
                    }

                    TraceData scopeLine = Traces[i].ScopeLine;

                    if (scopeLine == null)
                    {
                        continue;
                    }

                    if (scopeLine.DataPoints < 2)
                    {
                        continue;
                    }

                    gotOne = true;

                    min = Vector2.ComponentMin(min, scopeLine.Min);
                    max = Vector2.ComponentMax(max, scopeLine.Max);
                }
            }

            if (gotOne == false)
            {
                return;
            }

            AxesRange.YCenter = (max.Y - min.Y) * 0.5f + min.Y;

            if (doVerticalRange == true)
            {
                double range = (max.Y - min.Y) * GraphBase.RangePadding;

                range = MathHelper.Clamp(range, AxesRange.RangeMin, AxesRange.RangeMax);

                AxesRange.YRange = range;
            }

            if (AxesRange.YCenter != initialRange.YCenter ||
                AxesRange.YRange != initialRange.YRange)
            {
                UpdateGridLines();
            }
        }
    }
}