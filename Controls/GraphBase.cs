﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.LiteGL.Effect;
using Rug.LiteGL.Simple;
using TextRenderer = Rug.LiteGL.Text.TextRenderer;

namespace Rug.LiteGL.Controls
{
    public delegate void GraphViewPortChangedEvent(GraphBase graph, bool xChanged, bool yChanged);

    public abstract partial class GraphBase : UserControl
    {
        public AxesRange AxesRange = new AxesRange() { XMin = 0, XMax = 10, YMin = -1f, YMax = 1f };
        public AxesRange DefaultAxesRange = new AxesRange() { XMin = 0, XMax = 10, YMin = -1f, YMax = 1f };

        protected const float RangePadding = 1.2f;

        protected readonly bool IsInDesignMode;

        //protected ScopeLine[] ScopeLines = new ScopeLine[0];

        //protected Trace[] Traces = new Trace[0];

        private bool resourcesHaveBeenLoaded = false; 

        public readonly TraceCollection Traces = new TraceCollection(); 

        private TextureQuad backgroundFill;

        private Point dragStart;

        private Padding dynamicGraphInset;

        private SharedEffects effects = new SharedEffects();

        private GLControl_WithKeys glControl1;

        private GridLines gridLines_Horizontal;

        private GridLines gridLines_Vertical;

        private bool isDragging;

        private bool isInErrorState = false;

        private int labelStringLength;

        private GridLines otherLines;

        private Font resizedFont;

        private bool shouldUpdateGridLines = true;

        private bool shouldUpdateLegend;

        private TextRenderer textRenderer_Horizontal;

        private TextRenderer textRenderer_Legend;

        private TextRenderer textRenderer_Vertical;

        private TextureQuad texturedQuad_Horizontal;

        private TextureQuad texturedQuad_Legend;

        private TextureQuad texturedQuad_Vertical;

        private View3D view;

        [Category("Graph")]
        public Font AxisLabelFont { get; set; } = SystemFonts.DefaultFont;

        [Category("Graph")]
        public Color AxisValueColor { get; set; } = Color.FromArgb(255, 158, 158, 158);

        [Category("Graph")]
        public Color AxisValueMinorColor { get; set; } = Color.FromArgb(255, 64, 64, 64);

        [Category("Graph")]
        public Padding GraphInset { get; set; } = new Padding(64, 16, 16, 64);

        [Category("Graph")]
        [DefaultValue(false)]
        public bool LockXMouse { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool LockYMouse { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(true)]
        public bool ShowLegend { get; set; } = true;

        [Category("Graph")]
        [DefaultValue(true)]
        public bool ShowXAxisTitle { get; set; } = true;

        [Category("Graph")]
        [DefaultValue(true)]
        public bool ShowXAxisUnits { get; set; } = true;

        [Category("Graph")]
        [DefaultValue(true)]
        public bool ShowYAxisTitle { get; set; } = true;

        [Category("Graph")]
        [DefaultValue(true)]
        public bool ShowYAxisUnits { get; set; } = true;

        [Category("Graph")]
        [DefaultValue("")]
        public string XAxisLabel { get; set; } = string.Empty;

        [Category("Graph")]
        [DefaultValue("")]
        public string YAxisLabel { get; set; } = string.Empty;

        [Category("Graph")]
        [DefaultValue(12)]
        public int YAxisUnitsPadding { get; set; } = 12;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool ZoomFromXMin { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool ZoomFromYMin { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool ZoomXAndYTogether { get; set; } = false;

        private Vector2 PixelsPerUnit
        {
            get
            {
                int graphSizeWidth = Width - dynamicGraphInset.Horizontal;
                int graphSizeHeight = Height - dynamicGraphInset.Vertical;

                double pixelsPerUnitX = graphSizeWidth / AxesRange.XRange;
                double pixelsPerUnitY = graphSizeHeight / AxesRange.YRange;

                return new Vector2((float)pixelsPerUnitX, (float)pixelsPerUnitY);
            }
        }
        
        public event GraphViewPortChangedEvent UserChangedViewport;

        public event EventHandler ViewReset;

        public GraphBase()
        {
            IsInDesignMode = (LicenseManager.UsageMode == LicenseUsageMode.Designtime);

            InitializeComponent();

            base.SetStyle(ControlStyles.Selectable, true);

            if (IsInDesignMode == true)
            {
                return;
            }

            LoadOpenGLControl();

            effects.Effects.Add(SimpleTexturedQuad.GetName(BoxMode.Color), new SimpleTexturedQuad(BoxMode.Color));
            effects.Effects.Add(SimpleTexturedQuad.GetName(BoxMode.Textured), new SimpleTexturedQuad(BoxMode.Textured));
            effects.Effects.Add(SimpleTexturedQuad.GetName(BoxMode.TexturedColor), new SimpleTexturedQuad(BoxMode.TexturedColor));

            effects.Effects.Add(GraphLineEffect.GetName(), new GraphLineEffect());
            effects.Effects.Add(GridLineEffect.GetName(GridLineEffect.DottedMode.None), new GridLineEffect(GridLineEffect.DottedMode.None));
            effects.Effects.Add(GridLineEffect.GetName(GridLineEffect.DottedMode.Vertical), new GridLineEffect(GridLineEffect.DottedMode.Vertical));
            effects.Effects.Add(GridLineEffect.GetName(GridLineEffect.DottedMode.Horizontal), new GridLineEffect(GridLineEffect.DottedMode.Horizontal));

            gridLines_Vertical = new GridLines(effects, 1024);
            gridLines_Horizontal = new GridLines(effects, 1024);
            otherLines = new GridLines(effects, 12);

            backgroundFill = new TextureQuad(effects);

            textRenderer_Vertical = new TextRenderer(10, 10);
            texturedQuad_Vertical = new TextureQuad(effects);
            texturedQuad_Vertical.Texture = textRenderer_Vertical.Texture;

            textRenderer_Horizontal = new TextRenderer(10, 10);
            texturedQuad_Horizontal = new TextureQuad(effects);
            texturedQuad_Horizontal.Texture = textRenderer_Horizontal.Texture;

            textRenderer_Legend = new TextRenderer(10, 10);
            texturedQuad_Legend = new TextureQuad(effects);
            texturedQuad_Legend.Texture = textRenderer_Legend.Texture;

            view = new View3D(glControl1.Bounds, glControl1.Bounds.Width, glControl1.Bounds.Height, (float)Math.PI / 4f);

            dynamicGraphInset = GraphInset;

            Traces.Added += Traces_Added;
            Traces.Removed += Traces_Removed;
        }

        private void Traces_Removed(Trace trace)
        {
            lock (Traces.SyncRoot)
            {
                trace.SizeChanged -= Trace_SizeChanged;

                trace.ScopeLine?.UnloadResources();
                trace.ScopeLine = null;
            }
        }

        private void Traces_Added(Trace trace)
        {
            lock (Traces.SyncRoot)
            {
                trace.SizeChanged += Trace_SizeChanged;

                if (IsInDesignMode == true)
                {
                    return;
                }

                trace.ScopeLine = new TraceData(effects, (int)trace.MaxDataPoints, (int)trace.BlockSize) { Color = trace.Color };

                if (resourcesHaveBeenLoaded == true)
                {
                    trace.ScopeLine.LoadResources();
                }

                ConfigureTraces();
            }
        }

        private void Trace_SizeChanged(Trace trace)
        {
            lock (Traces.SyncRoot)
            {
                trace.ScopeLine?.UnloadResources();
                trace.ScopeLine = null;

                if (IsInDesignMode == true)
                {
                    return;
                }

                trace.ScopeLine = new TraceData(effects, (int)trace.MaxDataPoints, (int)trace.BlockSize) { Color = trace.Color };

                if (resourcesHaveBeenLoaded == true)
                {
                    trace.ScopeLine.LoadResources();
                }

                ConfigureTraces();
            }
        }

        public virtual void Clear()
        {
            lock (Traces.SyncRoot)
            {
                foreach (Trace trace in Traces)
                {
                    trace.ScopeLine?.Clear();
                }
            }

            UpdateGridLines();
        }

        public virtual void Clear(int index)
        {
            lock (Traces.SyncRoot)
            {
                TraceData scopeLine = Traces[index].ScopeLine;

                scopeLine?.Clear();
            }

            UpdateGridLines();
        }

        public void ResetView()
        {
            ViewReset?.Invoke(this, EventArgs.Empty);
            //AxesRange = DefaultAxesRange;

            UpdateGridLines();
        }

        public void ScrollGraph(float x, float y)
        {
            double centerX = AxesRange.XCenter;
            double centerY = AxesRange.YCenter;

            if (LockXMouse == false)
            {
                AxesRange.XCenter += x * AxesRange.XRange;

                UpdateGridLines();
            }

            if (LockYMouse == false)
            {
                AxesRange.YCenter += y * AxesRange.YRange;

                UpdateGridLines();
            }

            UserChangedViewport?.Invoke(this, centerX != AxesRange.XCenter, centerY != AxesRange.YCenter);
        }

        public void ZoomGraph(float x, float y)
        {
            double rangeX = AxesRange.XRange;
            double rangeY = AxesRange.YRange;

            if (x != 0 && x < 1f)
            {
                float delta = -x;

                if (delta < 0)
                {
                    delta = 1f + delta;
                }
                else
                {
                    delta = 1f / (1f - delta);
                }

                rangeX = AxesRange.XRange * delta;

                if (rangeX < 0.0001f || rangeX > 10000000f)
                {
                    rangeX = AxesRange.XRange;
                }

                AxesRange.XRange = rangeX;

                if (ZoomFromXMin == true)
                {
                    AxesRange.XCenter = AxesRange.XRange * 0.5f;
                }

                UpdateGridLines();
            }

            if (y != 0 && y < 1f)
            {
                float delta = y;

                if (delta < 0)
                {
                    delta = 1f + delta;
                }
                else
                {
                    delta = 1f / (1f - delta);
                }

                rangeY = AxesRange.YRange * delta;

                if (rangeY < 0.0001f || rangeY > 10000000f)
                {
                    rangeY = AxesRange.YRange;
                }

                AxesRange.YRange = rangeY;

                if (ZoomFromYMin == true)
                {
                    AxesRange.YCenter = AxesRange.YRange * 0.5f;
                }

                UpdateGridLines();
            }

            UserChangedViewport?.Invoke(this, rangeX != AxesRange.XRange, rangeY != AxesRange.YRange);
        }

        protected abstract void ConfigureTraces();

        protected abstract void OnPaintGraph();

        protected void UpdateGridLines()
        {
            shouldUpdateGridLines = true;
        }

        private static Font AppropriateFont(Graphics g, float minFontSize, float maxFontSize, Size layoutSize, string s, Font font, out SizeF extent)
        {
            if (maxFontSize == minFontSize)
            {
                font = new Font(font.FontFamily, minFontSize, font.Style);
            }

            extent = g.MeasureString(s, font);

            if (maxFontSize <= minFontSize)
                return font;

            float hRatio = layoutSize.Height / extent.Height;
            float wRatio = layoutSize.Width / extent.Width;
            float ratio = (hRatio < wRatio) ? hRatio : wRatio;

            float newSize = font.Size * ratio;

            if (newSize < minFontSize)
            {
                newSize = minFontSize;
            }
            else if (newSize > maxFontSize)
            {
                newSize = maxFontSize;
            }

            font = new Font(font.FontFamily, newSize, font.Style);

            extent = g.MeasureString(s, font);

            return font;
        }

        private static void GetMajorMinorTicks(ref double tickSize, ref int majorEveryInt, ref int labelEveryInt, double pixelsPerUnit, float pixelThreshold)
        {
            if (pixelsPerUnit * tickSize < pixelThreshold)
            {
                tickSize *= 10;

                if (pixelsPerUnit * tickSize < pixelThreshold * 2)
                {
                    majorEveryInt = 10;
                    labelEveryInt = 10;
                }
                else if (pixelsPerUnit * tickSize < pixelThreshold * 4)
                {
                    majorEveryInt = 5;
                    labelEveryInt = 5;
                }
                else if (pixelsPerUnit * tickSize < pixelThreshold * 8)
                {
                    majorEveryInt = 2;
                    labelEveryInt = 2;
                }
                else
                {
                    majorEveryInt = 1;
                    labelEveryInt = 1;
                }
            }
            else if (pixelsPerUnit * tickSize < pixelThreshold * 2)
            {
                tickSize *= 5;
                majorEveryInt = 2;
                labelEveryInt = 2;
            }
            else if (pixelsPerUnit * tickSize < pixelThreshold * 4)
            {
                tickSize *= 2;
                majorEveryInt = 5;
                labelEveryInt = 5;
            }
            else if (pixelsPerUnit * tickSize > pixelThreshold * 20)
            {
                labelEveryInt = 1;
            }
            else if (pixelsPerUnit * tickSize > pixelThreshold * 10)
            {
                labelEveryInt = 2;
            }
            else if (pixelsPerUnit * tickSize > pixelThreshold * 4)
            {
                labelEveryInt = 5;
            }
        }

        private void CheckForVerticalUnitLabelResize()
        {
            if (shouldUpdateGridLines == false)
            {
                return;
            }

            int maxSize = GetMaxVerticalUnitsLabelSize() + 4;

            int labelOffset = 0;

            if (ShowYAxisTitle == true)
            {
                float yAxisHeight = textRenderer_Vertical.Graphics.MeasureString(YAxisLabel, AxisLabelFont).Height;

                labelOffset += (int)yAxisHeight + YAxisUnitsPadding;
            }

            int newLeftInset = Math.Max(GraphInset.Left, labelOffset + maxSize);

            if (dynamicGraphInset.Left == newLeftInset)
            {
                return;
            }

            dynamicGraphInset.Left = newLeftInset;

            if (ShowYAxisUnits == true || ShowYAxisTitle == true)
            {
                textRenderer_Vertical.Resize(new Size(dynamicGraphInset.Left, glControl1.Bounds.Size.Height));
            }

            CreateOverLines();
            shouldUpdateLegend = true;
        }

        private void CreateOverLines()
        {
            int i = 0;

            Vector2 min = new Vector2(dynamicGraphInset.Left, dynamicGraphInset.Bottom) * view.PixelSize;
            Vector2 max = new Vector2(glControl1.Width - dynamicGraphInset.Right, glControl1.Height - dynamicGraphInset.Top) * view.PixelSize;

            float lineColor = 0.3f;

            if (min.X < max.X && min.Y < max.Y)
            {
                otherLines.Data[i++] = new Vector4(min.X, min.Y, 0.5f, lineColor);
                otherLines.Data[i++] = new Vector4(max.X, min.Y, 0.5f, lineColor);
                otherLines.Data[i++] = new Vector4(min.X, min.Y, 0.5f, lineColor);
                otherLines.Data[i++] = new Vector4(min.X, max.Y, 0.5f, lineColor);
                otherLines.Data[i++] = new Vector4(max.X, max.Y, 0.5f, lineColor);
                otherLines.Data[i++] = new Vector4(min.X, max.Y, 0.5f, lineColor);
                otherLines.Data[i++] = new Vector4(max.X, max.Y, 0.5f, lineColor);
                otherLines.Data[i++] = new Vector4(max.X, min.Y, 0.5f, lineColor);
            }

            otherLines.DataPoints = i;
            otherLines.Update();

            otherLines.Color = Color.White;
        }

        private void DisposeOpenGLControl()
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            Controls.Remove(glControl1);

            glControl1.Dispose();

            glControl1.Paint -= GlControl1_Paint;
            glControl1.Resize -= GlControl1_SizeChanged;
            //glControl1.KeyDown += glControl1_KeyDown;
            //glControl1.KeyPress += glControl1_KeyPress;
            //glControl1.KeyUp += glControl1_KeyUp;
            //glControl1.MouseClick += glControl1_MouseClick;
            glControl1.MouseDoubleClick -= GlControl1_MouseDoubleClick;
            glControl1.MouseDown -= GlControl1_MouseDown;
            glControl1.MouseMove -= GlControl1_MouseMove;
            glControl1.MouseUp -= GlControl1_MouseUp;
            glControl1.MouseWheel -= GlControl1_MouseWheel;

            glControl1.Load += GlControl1_Load;
            glControl1.Disposed += GlControl1_Disposed;
        }

        private string FormatLabel(float value, int decimalPlaces, bool addPlusSymbol)
        {
            string formatString = "f" + decimalPlaces.ToString();

            if (value >= 0 && addPlusSymbol == true)
            {
                return "+" + value.ToString(formatString);
            }
            else
            {
                return value.ToString(formatString);
            }
        }

        /// <summary>
        /// Timer tick event to refresh graphics.
        /// </summary>
        private void formUpdateTimer_Tick(object sender, EventArgs e)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            glControl1.Invalidate();
        }

        private int GetMaxVerticalUnitsLabelSize()
        {
            if (ShowYAxisUnits == false)
            {
                return 0;
            }

            int majorEveryInt = 10;
            int majorLineIndex = 0;

            int labelEveryInt = 10;
            int labelLineIndex = 0;

            double y;
            double tickSize;
            int decimalPlaces;
            AxesRange.GetMostSignificantTickY(out y, out tickSize, out decimalPlaces);
            double offsetY = AxesRange.YCenter;

            double pixelsPerUnit = PixelsPerUnit.Y;

            GetMajorMinorTicks(ref tickSize, ref majorEveryInt, ref labelEveryInt, pixelsPerUnit, 3);

            double yMax = AxesRange.YMax;
            double yMin = AxesRange.YMin;
            double xMin = AxesRange.XMin;
            double xMax = AxesRange.XMax;

            double maxUnitsLabelSize = 0;

            while (y <= yMax)
            {
                if (y >= yMin)
                {
                    if (majorLineIndex == 0)
                    {
                        maxUnitsLabelSize = Math.Max(maxUnitsLabelSize, textRenderer_Vertical.MeasureString(FormatLabel((float)y, decimalPlaces, false), this.Font, true, StringAlignment.Far).Width);
                    }
                    else if (labelLineIndex == 0)
                    {
                        maxUnitsLabelSize = Math.Max(maxUnitsLabelSize, textRenderer_Vertical.MeasureString(FormatLabel((float)y, decimalPlaces, false), this.Font, true, StringAlignment.Far).Width);
                    }
                }

                majorLineIndex = (majorLineIndex + 1) % majorEveryInt;
                labelLineIndex = (labelLineIndex + 1) % labelEveryInt;

                y += tickSize;
            }

            return (int)maxUnitsLabelSize;
        }

        private void GlControl1_Disposed(object sender, EventArgs e)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            try
            {
                resourcesHaveBeenLoaded = false; 

                effects.UnloadResources();

                lock (Traces.SyncRoot)
                {
                    foreach (Trace trace in Traces)
                    {
                        trace.ScopeLine?.UnloadResources();
                    }
                }

                gridLines_Vertical.UnloadResources();
                gridLines_Horizontal.UnloadResources();

                textRenderer_Vertical.UnloadResources();
                texturedQuad_Vertical.UnloadResources();

                textRenderer_Horizontal.UnloadResources();
                texturedQuad_Horizontal.UnloadResources();

                textRenderer_Legend.UnloadResources();
                texturedQuad_Legend.UnloadResources();

                otherLines.UnloadResources();
                backgroundFill.UnloadResources();
            }
            catch (Exception ex)
            {
                isInErrorState = true;
            }
        }

        private void GlControl1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    ScrollGraph(-0.1f, 0f);
                    break;

                case Keys.Right:
                    ScrollGraph(0.1f, 0f);
                    break;

                case Keys.Up:
                    ScrollGraph(0f, 0.1f);
                    break;

                case Keys.Down:
                    ScrollGraph(0f, -0.1f);
                    break;
            }
        }

        private void GlControl1_Load(object sender, EventArgs e)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            try
            {
                resourcesHaveBeenLoaded = true; 

                effects.LoadResources();

                lock (Traces.SyncRoot)
                {
                    foreach (Trace trace in Traces)
                    {
                        trace.ScopeLine?.LoadResources();
                    }
                }

                gridLines_Vertical.LoadResources();
                gridLines_Horizontal.LoadResources();

                textRenderer_Vertical.LoadResources();
                texturedQuad_Vertical.LoadResources();

                textRenderer_Horizontal.LoadResources();
                texturedQuad_Horizontal.LoadResources();

                textRenderer_Legend.LoadResources();
                texturedQuad_Legend.LoadResources();

                otherLines.LoadResources();
                backgroundFill.LoadResources();

                CreateOverLines();
                otherLines.Color = Color4.White;

                UpdateGridLines();
            }
            catch (Exception ex)
            {
                isInErrorState = true;
            }
        }

        private void GlControl1_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void GlControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            double centerX = AxesRange.XCenter;
            double centerY = AxesRange.YCenter;
            double rangeX = AxesRange.XRange;
            double rangeY = AxesRange.YRange;

            ResetView();

            //UserChangedViewport?.Invoke(this,
            //    rangeX != AxesRange.XRange || centerX != AxesRange.XCenter,
            //    rangeY != AxesRange.YRange || centerY != AxesRange.YCenter
            //    );
        }

        private void GlControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) != MouseButtons.Left)
            {
                return;
            }

            isDragging = true;
            dragStart = e.Location;
        }

        private void GlControl1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging == false)
            {
                return;
            }

            double centerX = AxesRange.XCenter;
            double centerY = AxesRange.YCenter;

            if (LockYMouse == false)
            {
                int delta = e.Location.Y - dragStart.Y;

                AxesRange.YCenter += delta / PixelsPerUnit.Y;
            }

            if (LockXMouse == false)
            {
                int delta = e.Location.X - dragStart.X;

                AxesRange.XCenter -= delta / PixelsPerUnit.X;
            }

            dragStart = e.Location;

            UserChangedViewport?.Invoke(this,
                centerX != AxesRange.XCenter,
                centerY != AxesRange.YCenter
                );

            UpdateGridLines();
        }

        private void GlControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) != MouseButtons.Left)
            {
                return;
            }

            isDragging = false;
        }

        private void GlControl1_MouseWheel(object sender, MouseEventArgs e)
        {
            float x = 0;
            float y = 0;

            if (ZoomXAndYTogether == true || Control.ModifierKeys == Keys.Control)
            {
                x = e.Delta < 0 ? -0.1f : 0.1f;
            }

            if (ZoomXAndYTogether == true || Control.ModifierKeys != Keys.Control)
            {
                y = e.Delta < 0 ? 0.1f : -0.1f;
            }

            ZoomGraph(x, y);
        }

        /// <summary>
        /// Redraw cuboid polygons.
        /// </summary>
        private void GlControl1_Paint(object sender, PaintEventArgs e)
        {
            if (isInErrorState == true)
            {
                return;
            }

            try
            {
                if (glControl1.Bounds.Width == 0 ||
                    glControl1.Bounds.Height == 0)
                {
                    return;
                }

                try
                {
                    glControl1.MakeCurrent();
                }
                catch (OpenTK.Graphics.GraphicsContextException ex)
                {
                    DisposeOpenGLControl();

                    LoadOpenGLControl();

                    return;
                }

                OnPaintGraph();

                view.UpdateProjection();

                GLState.CullFace(OpenTK.Graphics.OpenGL.CullFaceMode.Back);
                GLState.EnableCullFace = false;
                GLState.EnableDepthMask = false;
                GLState.EnableDepthTest = false;
                GLState.EnableBlend = false;
                GLState.ClearDepth(1.0f);
                GLState.Viewport = view.Viewport;

                GLState.ClearColor(Color.Black);
                GLState.ApplyAll(glControl1.Size);
                GL.Clear(ClearBufferMask.ColorBufferBit);

                CheckForVerticalUnitLabelResize();

                RenderInnerFrame();

                view.Resize(new Rectangle(0, 0, glControl1.Width, glControl1.Height), glControl1.Bounds.Width, glControl1.Bounds.Height);
                view.UpdateProjection();

                GLState.Viewport = view.Viewport;
                GLState.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
                GLState.EnableBlend = true;
                GLState.ApplyAll(glControl1.Size);

                if (ShowYAxisUnits == true || ShowYAxisTitle == true)
                {
                    texturedQuad_Vertical.Rectangle = new RectangleF(-1, -1, dynamicGraphInset.Left * view.PixelSize.X, 2f);

                    texturedQuad_Vertical.Render(view);
                }

                if (ShowXAxisUnits == true || ShowXAxisTitle == true)
                {
                    texturedQuad_Horizontal.Rectangle = new RectangleF(-1, -1f, 2f, dynamicGraphInset.Bottom * view.PixelSize.Y);

                    texturedQuad_Horizontal.Render(view);
                }

                if (ShowLegend == true)
                {
                    texturedQuad_Legend.Rectangle = new RectangleF(-1, 1f - (dynamicGraphInset.Top * view.PixelSize.Y), 2f, dynamicGraphInset.Top * view.PixelSize.Y);

                    texturedQuad_Legend.Render(view);
                }

                otherLines.Render(view, GridLineEffect.DottedMode.None, new Vector2(0, 0), new Vector2(1, 1), new Vector4(-1, -1, 1f, 1f));

                glControl1.SwapBuffers();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());

                //using (ExceptionDialog dialog = new ExceptionDialog())
                //{
                //    dialog.Title = "An Exception Occurred";

                //    dialog.Label = ex.Message;
                //    dialog.Detail = ex.ToString();

                //    dialog.ShowDialog(this);
                //}

                DisposeOpenGLControl();

                LoadOpenGLControl();

                //isInErrorState = true;
            }
        }

        /// <summary>
        /// Window resize event to adjusts perspective.
        /// </summary>
        private void GlControl1_SizeChanged(object sender, EventArgs e)
        {
            if (glControl1.Bounds.Width == 0 ||
                glControl1.Bounds.Height == 0)
            {
                return;
            }

            view.Resize(glControl1.Bounds, glControl1.Bounds.Width, glControl1.Bounds.Height);
            view.UpdateProjection();

            dynamicGraphInset = GraphInset;
            //dynamicGraphInset.Top += 12;

            int maxSize = GetMaxVerticalUnitsLabelSize() + 4;

            int labelOffset = 0;

            if (ShowYAxisTitle == true)
            {
                float yAxisHeight = textRenderer_Vertical.Graphics.MeasureString(YAxisLabel, AxisLabelFont).Height;

                labelOffset += (int)yAxisHeight + YAxisUnitsPadding;
            }

            if (dynamicGraphInset.Left < labelOffset + maxSize)
            {
                dynamicGraphInset.Left = labelOffset + maxSize;
            }

            if (ShowLegend == true)
            {
                Size legendSize = UpdateLegend(glControl1.Bounds.Size, true);
                dynamicGraphInset.Top += legendSize.Height;
                shouldUpdateLegend = true;

                textRenderer_Legend.Resize(new Size(glControl1.Bounds.Size.Width, dynamicGraphInset.Top));
            }

            if (ShowXAxisUnits == true || ShowXAxisTitle == true)
            {
                textRenderer_Horizontal.Resize(new Size(glControl1.Bounds.Size.Width, dynamicGraphInset.Bottom));
            }

            if (ShowYAxisUnits == true || ShowYAxisTitle == true)
            {
                textRenderer_Vertical.Resize(new Size(dynamicGraphInset.Left, glControl1.Bounds.Size.Height));
            }

            GLState.Viewport = view.Viewport;
            GLState.Apply(glControl1.Size);

            CreateOverLines();
            otherLines.Color = Color4.White;

            UpdateGridLines();
            shouldUpdateLegend = true;

            resizedFont?.Dispose();
            resizedFont = null;
        }

        private void Graph_Load(object sender, EventArgs e)
        {
            DefaultAxesRange = AxesRange;
        }

        private void LoadOpenGLControl()
        {
            glControl1 = new GLControl_WithKeys(new OpenTK.Graphics.GraphicsMode(32, 24, 0, 2))
            {
                Dock = DockStyle.Fill,
            };

            glControl1.Paint += GlControl1_Paint;
            glControl1.Resize += GlControl1_SizeChanged;
            //glControl1.KeyDown += glControl1_KeyDown;
            //glControl1.KeyPress += glControl1_KeyPress;
            //glControl1.KeyUp += glControl1_KeyUp;
            //glControl1.MouseClick += glControl1_MouseClick;
            glControl1.KeyDown += GlControl1_KeyDown;
            glControl1.MouseDoubleClick += GlControl1_MouseDoubleClick;
            glControl1.MouseDown += GlControl1_MouseDown;
            glControl1.MouseMove += GlControl1_MouseMove;
            glControl1.MouseUp += GlControl1_MouseUp;
            glControl1.MouseWheel += GlControl1_MouseWheel;

            glControl1.Load += GlControl1_Load;
            glControl1.Disposed += GlControl1_Disposed;

            Controls.Add(glControl1);
        }

        private void RenderInnerFrame()
        {
            Vector4 window = new Vector4(0, 0, 1f, 1f);

            Rectangle innerRectangle = new Rectangle(dynamicGraphInset.Left, dynamicGraphInset.Top, glControl1.Width - dynamicGraphInset.Horizontal, glControl1.Height - dynamicGraphInset.Vertical);

            if (innerRectangle.Width <= 0 || innerRectangle.Height <= 0)
            {
                return;
            }

            view.Resize(innerRectangle, glControl1.Bounds.Width, glControl1.Bounds.Height);
            view.UpdateProjection();

            if (shouldUpdateLegend == true && ShowLegend == true)
            {
                shouldUpdateLegend = false;
                UpdateLegend(glControl1.Bounds.Size, false);
            }

            if (shouldUpdateGridLines == true)
            {
                shouldUpdateGridLines = false;
                UpdateGridLineBuffers();
            }

            GLState.Viewport = view.Viewport;
            GLState.EnableBlend = false;
            GLState.ApplyAll(glControl1.Size);

            backgroundFill.Color = new Color4(0.06f, 0.06f, 0.06f, 1f);
            backgroundFill.ColorGradient = new Color4(0.01f, 0.01f, 0.01f, 1f);
            backgroundFill.GradientMode = true;
            backgroundFill.Render(view);

            double xScale = AxesRange.XScale;
            double yScale = AxesRange.YScale;
            double offsetY = -AxesRange.YCenter;
            double offsetX = -AxesRange.XCenter;

            gridLines_Vertical.Render(view, GridLineEffect.DottedMode.Vertical, new Vector2((float)offsetX, (float)offsetY), new Vector2((float)xScale, (float)yScale), window);
            gridLines_Horizontal.Render(view, GridLineEffect.DottedMode.Horizontal, new Vector2((float)offsetX, (float)offsetY), new Vector2((float)xScale, (float)yScale), window);

            offsetY = -(AxesRange.YCenter + AxesRange.YOffset);
            offsetX = -(AxesRange.XCenter + AxesRange.XOffset);

            lock (Traces.SyncRoot)
            {
                foreach (Trace trace in Traces)
                {
                    trace.ScopeLine?.Update();

                    trace.ScopeLine?.Render(view, new Vector2((float)offsetX, (float)offsetY), new Vector2((float)xScale, (float)yScale), window);
                }
            }
        }

        private void ResizeFont(Graphics g, string text, Size layoutSize, Font baseFont, ref Font resizedFont, out SizeF stringSize)
        {
            resizedFont?.Dispose();
            resizedFont = AppropriateFont(g, 5, 5000, layoutSize, text, baseFont, out stringSize);
        }

        private PointF ToScreenPoint(float x, float y, SizeF size)
        {
            y *= -1;

            x *= glControl1.Width * 0.5f;
            y *= glControl1.Height * 0.5f;

            x += glControl1.Width * 0.5f;
            y += glControl1.Height * 0.5f;

            y += size.Height * 0.1f;

            return new PointF(x, y);
        }

        private void UpdateGridLineBuffers()
        {
            if (ShowYAxisUnits == true || ShowYAxisTitle == true)
            {
                textRenderer_Vertical.Clear(Color.Black); 
            }

            if (ShowXAxisUnits == true || ShowXAxisTitle == true)
            {
                textRenderer_Horizontal.Clear(Color.Black); 
            }

            using (Brush axisValueColorBrush = new SolidBrush(AxisValueColor))
            using (Brush axisValueMinorColorBrush = new SolidBrush(AxisValueMinorColor))
            {
                int i = 0;

                float subTickColor = 0.125f; // 0.0625f;
                float subMajorTickColor = 0.175f; // 0.0625f;
                float majorTickColor = 0.3f;

                double xScale = AxesRange.XScale;
                double yScale = AxesRange.YScale;

                double fillX = 2d / xScale;
                double fillY = 2d / yScale;

                float pixelCenterY = dynamicGraphInset.Top + ((Height - dynamicGraphInset.Vertical) * 0.5f);
                float pixelCenterX = dynamicGraphInset.Left + ((Width - dynamicGraphInset.Horizontal) * 0.5f);
                float pixelStartX = dynamicGraphInset.Left;

                double pixelSizeX = 1d / (view.PixelSize.X / xScale);
                double pixelSizeY = 1d / (view.PixelSize.Y / yScale);

                textRenderer_Vertical.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;

                Graphics g = textRenderer_Vertical.Graphics;

                UpdateGridLines_Vertical(ref i, subTickColor, subMajorTickColor, majorTickColor, pixelCenterY, pixelSizeY, axisValueColorBrush, axisValueMinorColorBrush);

                if (ShowYAxisTitle == true)
                {
                    float yAxisHeight = g.MeasureString(YAxisLabel, AxisLabelFont).Height;

                    g.TranslateTransform(textRenderer_Vertical.Size.Width / 2, textRenderer_Vertical.Size.Height / 2);
                    g.RotateTransform(-90);
                    g.DrawString(YAxisLabel, AxisLabelFont, Brushes.White, (textRenderer_Vertical.Size.Height / 2) - pixelCenterY, (textRenderer_Vertical.Size.Width * -0.5f) + (yAxisHeight * 0.5f) + 8, textRenderer_Vertical.StringFormat_CenterCenter);
                    g.ResetTransform();
                }

                gridLines_Horizontal.DataPoints = i;
                gridLines_Horizontal.Update();

                gridLines_Horizontal.Color = Color.White;

                textRenderer_Horizontal.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;

                int fontSize = (int)textRenderer_Horizontal.Graphics.MeasureString("0", Font).Height + 4;

                float legendCenterY = fontSize + ((dynamicGraphInset.Bottom - fontSize) * 0.5f);

                UpdateGridLines_Horizontal(ref i, subTickColor, subMajorTickColor, majorTickColor, pixelStartX, pixelSizeX, fontSize, axisValueColorBrush, axisValueMinorColorBrush);

                if (ShowXAxisTitle == true)
                {
                    string labelText = XAxisLabel;

                    textRenderer_Horizontal.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit; // System.Drawing.Text.TextRenderingHint.AntiAlias;

                    textRenderer_Horizontal.DrawString(labelText, AxisLabelFont, Brushes.White, new PointF(pixelCenterX, legendCenterY), true);
                }

                gridLines_Vertical.DataPoints = i;
                gridLines_Vertical.Update();

                gridLines_Vertical.Color = Color.White;

                axisValueColorBrush.Dispose();

                textRenderer_Vertical.Update();
                textRenderer_Horizontal.Update();
            }
        }

        private int UpdateGridLines_Horizontal(ref int i, float subTickColor, float subMajorTickColor, float majorTickColor, double pixelStartX, double pixelSizeX, int fontSize, Brush axisValueColorBrush, Brush axisValueMinorColorBrush)
        {
            i = 0;

            double x;
            double tickSize;
            int decimalPlaces;

            AxesRange.GetMostSignificantTickX(out x, out tickSize, out decimalPlaces);

            int majorEveryInt = 10;
            int majorLineIndex = 0;

            int labelEveryInt = 10;
            int labelLineIndex = 0;

            double pixelsPerUnit = PixelsPerUnit.X;

            GetMajorMinorTicks(ref tickSize, ref majorEveryInt, ref labelEveryInt, pixelsPerUnit, 5);

            double offsetX = AxesRange.XMin;

            double yMax = AxesRange.YMax;
            double yMin = AxesRange.YMin;
            double xMin = AxesRange.XMin;
            double xMax = AxesRange.XMax;

            float textYPosition = fontSize * 0.5f;

            while (x <= xMax)
            {
                if (x >= xMin)
                {
                    float color = subTickColor;

                    if (majorLineIndex == 0)
                    {
                        color = majorTickColor;

                        if (ShowXAxisUnits == true)
                        {
                            textRenderer_Horizontal.DrawString(FormatLabel((float)x, decimalPlaces, true), this.Font, axisValueColorBrush, new PointF((float)(pixelStartX + (pixelSizeX * (x - offsetX))), textYPosition), true);
                        }
                    }
                    else if (labelLineIndex == 0)
                    {
                        color = subMajorTickColor;

                        if (ShowXAxisUnits == true)
                        {
                            textRenderer_Horizontal.DrawString(FormatLabel((float)x, decimalPlaces, true), this.Font, axisValueMinorColorBrush, new PointF((float)(pixelStartX + (pixelSizeX * (x - offsetX))), textYPosition), true);
                        }
                    }

                    gridLines_Vertical.Data[i++] = new Vector4((float)x, (float)yMin, 0.5f, color);
                    gridLines_Vertical.Data[i++] = new Vector4((float)x, (float)yMax, 0.5f, color);
                }

                majorLineIndex = (majorLineIndex + 1) % majorEveryInt;
                labelLineIndex = (labelLineIndex + 1) % labelEveryInt;

                x += tickSize;
            }

            return i;
        }

        private int UpdateGridLines_Vertical(ref int i, float subTickColor, float subMajorTickColor, float majorTickColor, double pixelCenterY, double pixelSizeY, Brush axisValueColorBrush, Brush axisValueMinorColorBrush)
        {
            i = 0;

            int majorEveryInt = 10;
            int majorLineIndex = 0;

            int labelEveryInt = 10;
            int labelLineIndex = 0;

            double y;
            double tickSize;
            int decimalPlaces;
            AxesRange.GetMostSignificantTickY(out y, out tickSize, out decimalPlaces);
            double offsetY = AxesRange.YCenter;

            double pixelsPerUnit = PixelsPerUnit.Y;

            GetMajorMinorTicks(ref tickSize, ref majorEveryInt, ref labelEveryInt, pixelsPerUnit, 3);

            double yMax = AxesRange.YMax;
            double yMin = AxesRange.YMin;
            double xMin = AxesRange.XMin;
            double xMax = AxesRange.XMax;

            while (y <= yMax)
            {
                if (y >= yMin)
                {
                    float color = subTickColor;

                    if (majorLineIndex == 0)
                    {
                        color = majorTickColor;

                        if (ShowYAxisUnits == true)
                        {
                            textRenderer_Vertical.DrawString(FormatLabel((float)y, decimalPlaces, false), this.Font, axisValueColorBrush, new PointF(dynamicGraphInset.Left - 4, (float)(pixelCenterY - (pixelSizeY * (y - offsetY)))), true, StringAlignment.Far);
                        }
                    }
                    else if (labelLineIndex == 0)
                    {
                        color = subMajorTickColor;

                        if (ShowYAxisUnits == true)
                        {
                            textRenderer_Vertical.DrawString(FormatLabel((float)y, decimalPlaces, false), this.Font, axisValueMinorColorBrush, new PointF(dynamicGraphInset.Left - 4, (float)(pixelCenterY - (pixelSizeY * (y - offsetY)))), true, StringAlignment.Far);
                        }
                    }

                    gridLines_Horizontal.Data[i++] = new Vector4((float)xMin, (float)y, 0.5f, color);
                    gridLines_Horizontal.Data[i++] = new Vector4((float)xMax, (float)y, 0.5f, color);
                }

                majorLineIndex = (majorLineIndex + 1) % majorEveryInt;
                labelLineIndex = (labelLineIndex + 1) % labelEveryInt;

                y += tickSize;
            }

            return i;
        }

        private Size UpdateLegend(Size windowSize, bool calculateSizeOnly)
        {
            if (calculateSizeOnly == false)
            {
                textRenderer_Legend.Clear(Color.Black);
            }

            float pixelStartX = dynamicGraphInset.Left;
            float legendOffset = pixelStartX;

            textRenderer_Legend.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
            textRenderer_Legend.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            float fontHeight = textRenderer_Legend.Graphics.MeasureString("0", Font).Height;

            float lineSize = fontHeight + 4;
            float sizeY = lineSize;
            float legendCenterY = (GraphInset.Top * 0.5f) + (lineSize * 0.5f);

            foreach (Trace trace in Traces)
            {
                string traceString = trace.Name;

                float labelSize = textRenderer_Legend.Graphics.MeasureString(traceString, Font).Width;

                labelSize += fontHeight * 1.125f;
                labelSize += fontHeight;

                if (legendOffset + labelSize > windowSize.Width)
                {
                    sizeY += lineSize;
                    legendCenterY += lineSize;
                    legendOffset = pixelStartX;
                }

                if (calculateSizeOnly == false)
                {
                    textRenderer_Legend.Graphics.FillEllipse(trace.Brush, new RectangleF(legendOffset, legendCenterY - (fontHeight * 0.5f), fontHeight, fontHeight));
                    legendOffset += fontHeight * 1.125f;
                    legendOffset += textRenderer_Legend.DrawString(traceString, Font, Brushes.White, new PointF(legendOffset, legendCenterY), true, StringAlignment.Near).Width + fontHeight;
                }
                else
                {
                    legendOffset += labelSize;
                }
            }

            if (calculateSizeOnly == false)
            {
                textRenderer_Legend.Update();
            }

            return new Size(windowSize.Width, (int)sizeY);
        }

        private class GLControl_WithKeys : GLControl
        {
            public GLControl_WithKeys(GraphicsMode mode) : base(mode)
            {
            }

            protected override bool IsInputKey(Keys keyData)
            {
                switch (keyData)
                {
                    case Keys.Right:
                    case Keys.Left:
                    case Keys.Up:
                    case Keys.Down:
                        return true;

                    case Keys.Shift | Keys.Right:
                    case Keys.Shift | Keys.Left:
                    case Keys.Shift | Keys.Up:
                    case Keys.Shift | Keys.Down:
                        return true;
                }

                return base.IsInputKey(keyData);
            }
        }
    }
}