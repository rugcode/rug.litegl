﻿using System;
using System.ComponentModel;
using OpenTK;
using Rug.LiteGL.Simple;

namespace Rug.LiteGL.Controls
{
    public class Graph : GraphBase
    {
        private DateTime baseTime;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool Rolling { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(false)]
        public bool TrackTrace { get; set; } = false;

        [Category("Graph")]
        [DefaultValue(-1)]
        public int TrackTraceIndex { get; set; } = -1;

        public Graph()
        {
            baseTime = DateTime.Now;
            XAxisLabel = "Seconds (" + Helper.DateTimeToString(baseTime, true) + ")";
        }

        public void AddScopeData(params float[] traceData)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            lock (Traces.SyncRoot)
            {
                float x = CheckForTimeReset();

                for (int i = 0, ie = Math.Min(traceData.Length, Traces.Count); i < ie; i++)
                {
                    Traces[i].ScopeLine?.AddDataPoint(new Vector2(x, traceData[i]));
                }
            }
        }

        public void AddScopeData(int index, float traceData)
        {
            if (IsInDesignMode == true)
            {
                return;
            }

            lock (Traces.SyncRoot)
            {
                float x = CheckForTimeReset();

                Traces[index].ScopeLine?.AddDataPoint(new Vector2(x, traceData));
            }
        }

        public void CenterOnTrace(int index)
        {
            CenterAndRangeOnTrace(index, false);
        }

        public override void Clear()
        {
            baseTime = DateTime.Now;

            base.Clear();
        }

        protected override void ConfigureTraces()
        {
            lock (Traces.SyncRoot)
            {
                foreach (Trace trace in Traces)
                {
                    if (trace.ScopeLine == null)
                    {
                        continue; 
                    }

                    trace.ScopeLine.AssumeLinearXValues = true;
                    trace.ScopeLine.RescanXForMinMaxOnBlockUpdate = true;
                }
            }
        }

        protected override void OnPaintGraph()
        {
            if (Rolling == true)
            {
                AxesRange.XCenter = (float)(DateTime.Now - baseTime).TotalSeconds - (AxesRange.XRange * 0.5f);

                UpdateGridLines();
            }

            if (TrackTrace == true)
            {
                CenterAndRangeOnTrace(TrackTraceIndex, true);
            }
        }

        private void CenterAndRangeOnTrace(int index, bool doRange)
        {
            lock (Traces.SyncRoot)
            {
                Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
                Vector2 max = new Vector2(float.MinValue, float.MinValue);

                bool gotOne = false;

                for (int i = 0; i < Traces.Count; i++)
                {
                    if (index != -1 && index != i)
                    {
                        continue;
                    }

                    TraceData scopeLine = Traces[i].ScopeLine;

                    if (scopeLine == null)
                    {
                        continue;
                    }

                    if (scopeLine.DataPoints < 2)
                    {
                        continue;
                    }

                    gotOne = true;

                    min = Vector2.ComponentMin(min, scopeLine.Min);
                    max = Vector2.ComponentMax(max, scopeLine.Max);
                }

                if (gotOne == true)
                {
                    AxesRange.YCenter = (max.Y - min.Y) * 0.5f + min.Y;

                    if (doRange == true)
                    {
                        double range = (max.Y - min.Y) * GraphBase.RangePadding;

                        range = MathHelper.Clamp(range, AxesRange.RangeMin, AxesRange.RangeMax);

                        AxesRange.YRange = range;
                    }

                    UpdateGridLines();
                }
            }
        }

        private float CheckForTimeReset()
        {
            DateTime now = DateTime.Now;

            float x = (float)(now - baseTime).TotalSeconds;

            if (Rolling == true)
            {
                return x;
            }

            double xScale = AxesRange.XScale;

            if (x > 2f / xScale)
            {
                baseTime = now;

                XAxisLabel = "Seconds (" + Helper.DateTimeToString(baseTime, true) + ")";

                lock (Traces.SyncRoot)
                {
                    foreach (Trace trace in Traces)
                    { 
                        trace.ScopeLine?.Clear();
                    }
                }

                x = 0;

                base.UpdateGridLines();
            }

            return x;
        }
    }
}