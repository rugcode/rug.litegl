﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;
using Rug.LiteGL.Simple;

namespace Rug.LiteGL.Controls
{
    public delegate void TraceEvent(Trace trace);

    public class Trace : ICloneable
    {
        public const uint DefaultBlockSize = 256;
        public const uint DefaultMaxDataPoints = 1024 * 24;

        private uint blockSize = DefaultBlockSize;
        private Color color;
        private uint maxDataPoints = DefaultMaxDataPoints;

        public uint BlockSize
        {
            get { return blockSize; }

            set
            {
                blockSize = value;

                SizeChanged?.Invoke(this);
            }
        }

        public Color Color
        {
            get { return color; }

            set
            {
                color = value;

                Brush?.Dispose();

                Brush = new SolidBrush(color);
            }
        }

        public string DataSource { get; set; }

        public uint MaxDataPoints
        {
            get { return maxDataPoints; }

            set
            {
                maxDataPoints = value;

                SizeChanged?.Invoke(this);
            }
        }

        public string Name { get; set; }

        internal Brush Brush { get; private set; }

        internal TraceData ScopeLine { get; set; }

        internal event TraceEvent SizeChanged;

        public Trace Clone()
        {
            Trace newTrace = new Trace()
            {
                Name = Name,
                Color = Color,
                DataSource = DataSource,
                BlockSize = BlockSize,
                MaxDataPoints = MaxDataPoints,
            };

            return newTrace;
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public void Load(XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, nameof(Name), string.Empty);

            if (string.IsNullOrEmpty(Name) == true)
            {
                throw new Exception(string.Format("Missing {0} attribute.", nameof(Name)));
            }

            Color = Rug.LiteGL.Helper.GetAttributeValue(node, nameof(Color), Color);
            DataSource = Helper.GetAttributeValue(node, nameof(DataSource), DataSource);

            BlockSize = Helper.GetAttributeValue(node, nameof(BlockSize), DefaultBlockSize);
            MaxDataPoints = Helper.GetAttributeValue(node, nameof(MaxDataPoints), DefaultMaxDataPoints);
        }

        public XmlElement Save(XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Name), Name);
            Helper.AppendAttributeAndValue(element, nameof(Color), Color);
            Helper.AppendAttributeAndValue(element, nameof(DataSource), DataSource);
            Helper.AppendAttributeAndValue(element, nameof(BlockSize), BlockSize);
            Helper.AppendAttributeAndValue(element, nameof(MaxDataPoints), MaxDataPoints);

            return element;
        }
    }

    public class TraceCollection : ICollection<Trace>, IEnumerable<Trace>, IEnumerable
    {
        public readonly object SyncRoot = new object();

        private readonly List<Trace> traces = new List<Trace>();

        public int Count { get { return traces.Count; } }

        public bool IsReadOnly { get { return ((IList<Trace>)traces).IsReadOnly; } }

        public Trace this[int index] { get { return traces[index]; } }

        public event TraceEvent Added;

        public event TraceEvent Removed;

        public void Add(Trace item)
        {
            lock (SyncRoot)
            {
                traces.Add(item);
            }

            Added?.Invoke(item);
        }

        public void AddRange(IEnumerable<Trace> traces)
        {
            foreach (Trace trace in traces)
            {
                Add(trace);
            }
        }

        public void Clear()
        {
            List<Trace> removed;

            lock (SyncRoot)
            {
                removed = new List<Trace>(traces);
                traces.Clear();
            }

            foreach (Trace trace in removed)
            {
                Removed?.Invoke(trace);
            }
        }

        public bool Contains(Trace item)
        {
            lock (SyncRoot)
            {
                return traces.Contains(item);
            }
        }

        public void CopyTo(Trace[] array, int arrayIndex)
        {
            lock (SyncRoot)
            {
                traces.CopyTo(array, arrayIndex);
            }
        }

        public IEnumerator<Trace> GetEnumerator()
        {
            return ((IList<Trace>)traces).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<Trace>)traces).GetEnumerator();
        }

        public int IndexOf(Trace item)
        {
            lock (SyncRoot)
            {
                return traces.IndexOf(item);
            }
        }

        public void Insert(int index, Trace item)
        {
            lock (SyncRoot)
            {
                traces.Insert(index, item);
            }
        }

        public bool Remove(Trace item)
        {
            bool removed;

            lock (SyncRoot)
            {
                removed = traces.Remove(item);
            }

            if (removed == true)
            {
                Removed?.Invoke(item);
            }

            return removed;
        }

        public void RemoveAt(int index)
        {
            Trace removed;

            lock (SyncRoot)
            {
                removed = this[index];

                traces.RemoveAt(index);
            }

            if (removed != null)
            {
                Removed?.Invoke(removed);
            }
        }
    }
}