﻿using System;
using System.Drawing;

namespace Rug.LiteGL.Text
{
    public class TextRenderer : IDisposable
    {
        private readonly object syncLock = new object(); 

        Bitmap bitmap;
        
        Rectangle dirtyRegion;

        public readonly BitmapTexture2D Texture;

        public Size Size { get; private set; } 

        public Graphics Graphics { get; private set; }

        public readonly StringFormat StringFormat_CenterCenter;
        public readonly StringFormat StringFormat_FarCenter;
        public readonly StringFormat StringFormat_NearCenter;

        public TextRenderer(int width, int height)
        {
            if (width <= 0)
            {
                throw new ArgumentOutOfRangeException("width");
            }

            if (height <= 0)
            {
                throw new ArgumentOutOfRangeException("height");
            }

            StringFormat_CenterCenter = new StringFormat();
            StringFormat_CenterCenter.Alignment = StringAlignment.Center;
            StringFormat_CenterCenter.LineAlignment = StringAlignment.Center;

            StringFormat_FarCenter = new StringFormat();
            StringFormat_FarCenter.Alignment = StringAlignment.Far;
            StringFormat_FarCenter.LineAlignment = StringAlignment.Center;

            StringFormat_NearCenter = new StringFormat();
            StringFormat_NearCenter.Alignment = StringAlignment.Near;
            StringFormat_NearCenter.LineAlignment = StringAlignment.Center;

            Size = new Size(width, height);
            Texture = new BitmapTexture2D("Texture", bitmap, Texture2DInfo.Bitmap32_NearestClampToEdge(Size));

            Resize(Size); 
        }

        public void Resize(Size newSize)
        {
            lock (syncLock)
            {
                if (newSize.Width <= 0)
                {
                    return;
                    //throw new ArgumentOutOfRangeException("width");
                }

                if (newSize.Height <= 0)
                {
                    return;
                    //throw new ArgumentOutOfRangeException("height");
                }

                bool wasLoaded = Texture.IsLoaded;

                Dispose();

                Size = newSize;

                bitmap = new Bitmap(newSize.Width, newSize.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Graphics = Graphics.FromImage(bitmap);
                Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

                Texture.Image = bitmap;
                Texture.ResourceInfo.Size = Size;

                if (wasLoaded == true)
                {
                    LoadResources();
                }

                dirtyRegion = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            }
        }

        public void Clear(Color color)
        {
            lock (syncLock)
            {
                Graphics.Clear(color);
                dirtyRegion = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            }
        }

        public SizeF MeasureString(string text, Font font, bool lineAlignCenter, StringAlignment alignment)
        {
            lock (syncLock)
            {
                if (lineAlignCenter == true)
                {
                    StringFormat stringFormat;

                    switch (alignment)
                    {
                        case StringAlignment.Near:
                            stringFormat = StringFormat_NearCenter;
                            break;
                        case StringAlignment.Far:
                            stringFormat = StringFormat_FarCenter;
                            break;
                        default:
                            stringFormat = StringFormat_CenterCenter;
                            break;
                    }

                    return Graphics.MeasureString(text, font, 10000, stringFormat);
                }
                else
                {
                    return Graphics.MeasureString(text, font);
                }
            }
        }

        public SizeF DrawString(string text, Font font, Brush brush, PointF point, bool lineAlignCenter = true, StringAlignment alignment = StringAlignment.Center)
        {
            lock (syncLock)
            {
                //GuiTextRenderer.DrawText(Graphics, text, font, Point.Truncate(point), color, Color.Transparent, TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.GlyphOverhangPadding);

                SizeF size;

                //Graphics.FillEllipse(Brushes.White, point.X - 2, point.Y - 2, 4f, 4f); 

                if (lineAlignCenter == true)
                {
                    StringFormat stringFormat;

                    switch (alignment)
                    {
                        case StringAlignment.Near:
                            stringFormat = StringFormat_NearCenter;
                            break;
                        case StringAlignment.Far:
                            stringFormat = StringFormat_FarCenter;
                            break;
                        default:
                            stringFormat = StringFormat_CenterCenter;
                            break;
                    }


                    Graphics.DrawString(text, font, brush, point, stringFormat);

                    size = Graphics.MeasureString(text, font, 10000, stringFormat);

                    //Graphics.DrawRectangle(Pens.White, point.X - (size.Width * 0.5f), point.Y - (size.Height * 0.5f), size.Width, size.Height);
                }
                else
                {
                    Graphics.DrawString(text, font, brush, point);

                    size = Graphics.MeasureString(text, font);

                    //Graphics.DrawRectangle(Pens.White, point.X, point.Y, size.Width, size.Height);
                }

                if (dirtyRegion == Rectangle.Empty)
                {
                    dirtyRegion = Rectangle.Inflate(Rectangle.Round(new RectangleF(point, size)), 100, 100);
                }
                else
                {
                    dirtyRegion = Rectangle.Round(RectangleF.Union(dirtyRegion, RectangleF.Inflate(new RectangleF(point, size), 100, 100)));
                }

                dirtyRegion = Rectangle.Intersect(dirtyRegion, new Rectangle(0, 0, bitmap.Width, bitmap.Height));

                return size;
            }
        }

        public void Update()
        {
            lock (syncLock)
            {
                if (dirtyRegion != RectangleF.Empty)
                {
                    try
                    {
                        Texture.UploadImage(ref dirtyRegion);

                        dirtyRegion = Rectangle.Empty;
                    }
                    catch
                    {

                    }
                }
            }
        }

        public void LoadResources()
        {
            lock (syncLock)
            {
                Texture.LoadResources();
            }
        }

        public void UnloadResources()
        {
            lock (syncLock)
            {
                Texture.UnloadResources();
            }
        }

        public void Dispose()
        {
            lock (syncLock)
            {
                UnloadResources();

                if (Graphics != null)
                {
                    Graphics.Dispose();
                }

                if (bitmap != null)
                {
                    bitmap.Dispose();
                }
            }
        }
    }
}
