﻿namespace Rug.LiteGL.Effect
{
    public interface IEffect
    {
        bool HasChanged { get; }

        bool IsLoaded { get; }

        ProgramState State { get; }

        int ProgramHandle { get; }

        void LoadResources();
        void UnloadResources();
    }
}
