﻿using System;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Effect
{
    public class ModelSceneLight
    {
        public const float MinimumLight = 0.01f;

        public object Tag { get; set; }

        public Vector3 Center { get; set; }

        public Color4 Color { get; set; }

        public float SpecularPower { get; set; }

        public float AttenuatuionTermA { get; set; }

        public float Radius { get; set; }

        public bool IsInfinite { get { return float.IsInfinity(Radius); } }

        public float Intensity { get; set; }

        public bool Directional { get; set; }

        public Color4 ColorArg { get; private set; }

        public ModelLightAttenuation Attenuation { get; private set; }

        public float BoundsRadius { get; private set; }

        public ModelSceneLight()
        {
            Intensity = 1f;
            AttenuatuionTermA = 0f;
            Radius = float.PositiveInfinity;
        }

        public void Update()
        {
            if (IsInfinite == true)
            {
                Attenuation = new ModelLightAttenuation(0f, 0f);

                BoundsRadius = float.PositiveInfinity;
            }
            else
            {
                float b = 1.0f / (Radius * Radius * MinimumLight);

                Attenuation = new ModelLightAttenuation(AttenuatuionTermA, b);

                BoundsRadius = Math.Max(Radius, Radius * SpecularPower) * Intensity;
            }

            ColorArg = new Color4(Color.R, Color.G, Color.B, SpecularPower);
        }

        public ModelLightInstance ToLightInstance(ref Matrix4 world, ref Matrix4 normalWorld)
        {
            Vector3 center;

            if (Directional == true)
            {
                center = Vector3.Transform(Center, normalWorld);
            }
            else
            {
                center = Vector3.Transform(Center, world);
            }

            return new ModelLightInstance()
            {
                Position = center, // Center,
                Intensity = Intensity,
                Color = ColorArg,
                Attenuation = Attenuation,
            };
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ModelLightAttenuation
    {
        public float A;
        public float B;
        public float Padding1;
        public float Padding2;

        public ModelLightAttenuation(float a, float b)
        {
            A = a;
            B = b;
            Padding1 = 0;
            Padding2 = 0;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct ModelLightInstance
    {
        public static ModelLightInstance Zero = new ModelLightInstance()
        {
            Position = Vector3.Zero,
            Intensity = 0f,
            Color = new Color4(0.0f, 0.0f, 0.0f, 0.0f),
            Attenuation = new ModelLightAttenuation(0.0f, 0.0f),
        };


        [FieldOffset(0)]
        public Vector3 Position;

        [FieldOffset(12)]
        public float Intensity;

        [FieldOffset(16)]
        public Color4 Color;

        [FieldOffset(32)]
        public ModelLightAttenuation Attenuation;

        public enum Elements : int { Position = 0, Intensity = 1, Color = 2, Attenuation };

        public static readonly int Stride;

        public static readonly int PositionOffset;
        public static readonly int IntensityOffset;
        public static readonly int ColorOffset;
        public static readonly int AttenuationOffset;

        static ModelLightInstance()
        {
            Stride = BlittableValueType<ModelLightInstance>.Stride;

            PositionOffset = (int)Marshal.OffsetOf(typeof(ModelLightInstance), "Position");
            IntensityOffset = (int)Marshal.OffsetOf(typeof(ModelLightInstance), "Intensity");
            ColorOffset = (int)Marshal.OffsetOf(typeof(ModelLightInstance), "Color");
            AttenuationOffset = (int)Marshal.OffsetOf(typeof(ModelLightInstance), "Attenuation");

            if (AttenuationOffset + BlittableValueType<ModelLightAttenuation>.Stride != Stride)
            {
                throw new Exception("Stride does not match offset total");
            }
        }

        public static void Bind()
        {
            GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
            GL.VertexAttribPointer((int)Elements.Intensity, 1, VertexAttribPointerType.Float, false, Stride, IntensityOffset);
            GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
            GL.VertexAttribPointer((int)Elements.Attenuation, 4, VertexAttribPointerType.Float, false, Stride, AttenuationOffset);

            GL.EnableVertexAttribArray((int)Elements.Position);
            GL.EnableVertexAttribArray((int)Elements.Intensity);
            GL.EnableVertexAttribArray((int)Elements.Color);
            GL.EnableVertexAttribArray((int)Elements.Attenuation);
        }

        public static void Unbind()
        {
            GL.DisableVertexAttribArray((int)Elements.Position);
            GL.DisableVertexAttribArray((int)Elements.Intensity);
            GL.DisableVertexAttribArray((int)Elements.Color);
            GL.DisableVertexAttribArray((int)Elements.Attenuation);
        }

        public readonly static IVertexFormat Format = new FormatInfo();

        #region Format Class

        private class FormatInfo : IVertexFormat
        {
            #region IVertexFormat Members

            public int Stride
            {
                get { return ModelLightInstance.Stride; }
            }

            public void CreateLayout(ref int baseLocation)
            {
                GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
                GL.VertexAttribPointer((int)Elements.Intensity + baseLocation, 1, VertexAttribPointerType.Float, false, Stride, IntensityOffset);
                GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
                GL.VertexAttribPointer((int)Elements.Attenuation + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, AttenuationOffset);

                GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Intensity + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Attenuation + baseLocation);

                baseLocation += 4;
            }

            public void CreateLayout(ref int baseLocation, int devisor)
            {
                GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
                GL.VertexAttribPointer((int)Elements.Intensity + baseLocation, 1, VertexAttribPointerType.Float, false, Stride, IntensityOffset);
                GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
                GL.VertexAttribPointer((int)Elements.Attenuation + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, AttenuationOffset);

                GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Intensity + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Attenuation + baseLocation);

                GL.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
                GL.VertexAttribDivisor((int)Elements.Intensity + baseLocation, devisor);
                GL.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);
                GL.VertexAttribDivisor((int)Elements.Attenuation + baseLocation, devisor);

                baseLocation += 4;
            }

            #endregion
        }

        #endregion
    }
}
