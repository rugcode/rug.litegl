﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.LiteGL.Buffers;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Effect
{
    public class GridLineEffect : BasicEffectBase
	{
        public enum DottedMode
        {
            None = 0,
            Vertical = 1,
            Horizontal = 2,
        } 

        public static string GetName(DottedMode mode)
        {
            return "Grid Lines (" + mode.ToString() + ")";
        }

		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/GridLine";

        private int uColor;
        private int uOffsetAndScale;
        private int uWindow;

        #endregion

        public override string Name { get { return GetName(Dotted); } }

		public override string ShaderLocation { get { return m_ShaderLocation; } }

        public readonly DottedMode Dotted; 

        public GridLineEffect(DottedMode dotted) 
        {
            Dotted = dotted;

            switch (dotted)
            {
                case DottedMode.None:
                    break;
                case DottedMode.Vertical:
                    Defines = new string[] { "Mode_Vertical" }; 
                    break;
                case DottedMode.Horizontal:
                    Defines = new string[] { "Mode_Horizontal" };            
                    break;
                default:
                    break;
            }
        }

        public void Render(int index, int count, VertexBuffer vertex, Color4 color, Vector2 offset, Vector2 scale, Vector4 window)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            if (vertex.ResourceInfo.Format != GridLineVertex.Format)
            {
                throw new Exception("Incorrect vertex format '" + vertex.ResourceInfo.Format.GetType().ToString() + "'");
            }

            GL.UseProgram(ProgramHandle);


            GL.Uniform4(uColor, color);
            GL.Uniform4(uOffsetAndScale, new Vector4(offset.X, offset.Y, scale.X, scale.Y));
            GL.Uniform4(uWindow, window); 

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
            GridLineVertex.Bind();

            GL.DrawArrays(PrimitiveType.Lines, index, count);

            GL.UseProgram(0);

            GridLineVertex.Unbind();
        }

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

            uColor = GL.GetUniformLocation(ProgramHandle, "color");
            uOffsetAndScale = GL.GetUniformLocation(ProgramHandle, "offsetAndScale");
            uWindow = GL.GetUniformLocation(ProgramHandle, "window");

            GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion

    }
}
