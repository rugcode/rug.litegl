﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using OpenTK.Graphics.OpenGL;

namespace Rug.LiteGL.Effect
{
    public enum ProgramState
	{
		NotLoaded, 
		Loaded, 
		Linked, 
		Error, 
	}

	public abstract class BasicEffectBase : IEffect
	{
		private int m_ProgramHandle;
		private ProgramState m_State = ProgramState.NotLoaded;

		public ProgramState State { get { return m_State; } } 

		public abstract string ShaderLocation { get; }

		public DateTime VertexTimeStamp { get; set; }
		public DateTime FragmentTimeStamp { get; set; }

        public string ShaderSource_Vert { get; private set; }
        public string ShaderSource_Frag { get; private set; } 

		public bool HasChanged
		{
			get
			{
                DateTime vertexTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation + ".vert.glsl");
                DateTime fragmentTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation + ".frag.glsl");

				if (VertexTimeStamp != vertexTimeStamp)
				{
					return true; 
				}

				if (FragmentTimeStamp != fragmentTimeStamp)
				{
					return true;
				}

				return false; 
			}
		}

		public string[] Defines { get; set; }

		public readonly Dictionary<string, int> Loops = new Dictionary<string, int>();

		public int ProgramHandle
		{
			get
			{
				return m_ProgramHandle;
			}
		}

		public BasicEffectBase()
		{
			Defines = new string[0]; 
		}

		public void Load()
		{
			if (m_State != ProgramState.NotLoaded)
			{
				throw new Exception("Program '" + Name + "' is not is a state that will allow loading (" + m_State + ")"); 
			}

			m_ProgramHandle = GL.CreateProgram();

            string[] defines = new string[Defines.Length + 1];

            Defines.CopyTo(defines, 0);
            defines[Defines.Length] = "Pass_Vertex";

            ShaderSource_Vert = ShaderParser.Parse(ShaderLocation + ".vert.glsl", defines, Loops);

            VertexTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation + ".vert.glsl"); 

			int vertexShader = GL.CreateShader(ShaderType.VertexShader);
			GL.ShaderSource(vertexShader, ShaderSource_Vert);
			GL.CompileShader(vertexShader);

			string log = GL.GetShaderInfoLog(vertexShader);

			if (String.IsNullOrEmpty(log) == false && log.StartsWith("No errors.") == false)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("Error loading vertex shader: " + Name);
                sb.AppendLine(ShaderLocation + ".vert.glsl");
                sb.AppendLine(log);

                m_State = ProgramState.Error;

                throw new Exception(sb.ToString()); 
			}


            Defines.CopyTo(defines, 0);
            defines[Defines.Length] = "Pass_Fragment";

            ShaderSource_Frag = ShaderParser.Parse(ShaderLocation + ".frag.glsl", defines, Loops);

            FragmentTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation + ".frag.glsl"); 

			int fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
			GL.ShaderSource(fragmentShader, ShaderSource_Frag);
			GL.CompileShader(fragmentShader);

            log = GL.GetShaderInfoLog(fragmentShader);

			if (String.IsNullOrEmpty(log) == false && log.StartsWith("No errors.") == false)
			{
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("Error loading fragment shader: " + Name);
                sb.AppendLine(ShaderLocation + ".frag.glsl");
                sb.AppendLine(log);

                m_State = ProgramState.Error;

                throw new Exception(sb.ToString());
            }

            WriteResolvedShaders();

            if (m_State == ProgramState.Error)
            {
                return; 
            }

			GL.AttachShader(m_ProgramHandle, vertexShader);
			GL.AttachShader(m_ProgramHandle, fragmentShader);

			GL.DeleteShader(vertexShader);
			GL.DeleteShader(fragmentShader);

			m_State = ProgramState.Loaded; 
		}

        [Conditional("DEBUG")] 
        private void WriteResolvedShaders()
        {
            Helper.EnsurePathExists(Helper.ResolvePath("~/ShaderOutput/"));

            System.IO.File.WriteAllText(Helper.ResolvePath("~/ShaderOutput/" + Name.Replace(":", "_") + ".vert.glsl"), ShaderSource_Vert);
            System.IO.File.WriteAllText(Helper.ResolvePath("~/ShaderOutput/" + Name.Replace(":", "_") + ".frag.glsl"), ShaderSource_Frag);
        }


		public void Link()
		{
			if (m_State != ProgramState.Loaded)
			{
				throw new Exception("Shader program '" + Name + "' is not is a state that will allow linking (" + m_State + ")");
			}

			GL.LinkProgram(m_ProgramHandle);

			string log = GL.GetProgramInfoLog(m_ProgramHandle);

            if (String.IsNullOrEmpty(log) == false && log.StartsWith("No errors.") == false)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("Error linking shader: " + Name);
                sb.AppendLine(log);

                m_State = ProgramState.Error;

                throw new Exception(sb.ToString());
            }
			else
			{
				m_State = ProgramState.Linked;
			}
		}

		#region IResourceManager Members

		public virtual void LoadResources()
		{
			Load();

			OnLoadResources();

			Link(); 
		}

		protected abstract void OnLoadResources();

		public virtual void UnloadResources()
		{
			OnUnloadResources();

			if (m_ProgramHandle != 0)
			{
				GL.DeleteProgram(m_ProgramHandle);

				m_ProgramHandle = 0;

				m_State = ProgramState.NotLoaded;
			}
		}

		protected abstract void OnUnloadResources(); 

		#endregion

		public abstract string Name { get; }

		public uint ResourceHandle { get { return 0; } }

		public bool IsLoaded
		{
			get { return State == ProgramState.Linked; }
		}
	}
}
