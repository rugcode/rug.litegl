﻿using System;
using System.Collections.Generic;

namespace Rug.LiteGL.Effect
{
    public class SharedEffects
	{
		public readonly Dictionary<string, IEffect> Effects = new Dictionary<string, IEffect>();

		private bool m_Disposed = true;

		public SharedEffects()
		{
			
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				foreach (IEffect manager in Effects.Values)
				{
					manager.LoadResources();
				}

				m_Disposed = false;
			}
		}

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				foreach (IEffect manager in Effects.Values)
				{
					manager.UnloadResources();
				}
				m_Disposed = true;
			}
		}

		public bool Disposed { get { return m_Disposed; } }

		public void Dispose()
		{
			foreach (IEffect manager in Effects.Values)
			{
				manager.UnloadResources();

				if (manager is IDisposable)
				{
					(manager as IDisposable).Dispose(); 
				}
			}

			Effects.Clear();

			m_Disposed = true;
		}

		public void ReloadIfNeeded()
		{
			foreach (IEffect manager in Effects.Values)
			{
                if (manager.HasChanged == true)
				{
					manager.UnloadResources();
					
					manager.LoadResources();
				}
			}			
		}
	}
}
