﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.LiteGL.Buffers;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Effect
{
    public class GraphLineEffect : BasicEffectBase
	{
        public static string GetName()
        {
            return "Graph Lines";
        }

		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/GraphLine";

        private int uColor;
        private int uOffsetAndScale;
        private int uWindow;

        #endregion

        public override string Name { get { return GetName(); } }

		public override string ShaderLocation { get { return m_ShaderLocation; } }

        public GraphLineEffect() 
        {

        }

        public void Render(int index, int count, VertexBuffer vertex, Color4 color, Vector2 offset, Vector2 scale, Vector4 window)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            if (vertex.ResourceInfo.Format != GraphLineVertex.Format)
            {
                throw new Exception("Incorrect vertex format '" + vertex.ResourceInfo.Format.GetType().ToString() + "'");
            }

            GL.UseProgram(ProgramHandle);


            GL.Uniform4(uColor, color);
            GL.Uniform4(uOffsetAndScale, new Vector4(offset.X, offset.Y, scale.X, scale.Y));
            GL.Uniform4(uWindow, window); 

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
            GraphLineVertex.Bind();

            GL.DrawArrays(PrimitiveType.LineStrip, index, count);

            GL.UseProgram(0);

            GraphLineVertex.Unbind();
        }

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

            uColor = GL.GetUniformLocation(ProgramHandle, "color");
            uOffsetAndScale = GL.GetUniformLocation(ProgramHandle, "offsetAndScale");
            uWindow = GL.GetUniformLocation(ProgramHandle, "window");

            GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion

    }
}
