﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.LiteGL.Buffers;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Effect
{
    public class LightingArguments
    {
        public Matrix4 WorldMatrix = Matrix4.Identity;
        public Matrix4 NormalWorldMatrix = Matrix4.Identity;
        public Matrix4 ProjectionMatrix = Matrix4.Identity;
    }

    public class SimpleTexturedMaterial : BasicEffectBase
    {
        private uint uModelLightBufferIndex = 0;
        private int uObjectMatrix;
        private int uNormalMatrix;
        private int uInstanceColor;
        //private int uCameraCenter;
        private int uWorldMatrix;
        private int uNormalWorldMatrix;
        private int uPerspectiveMatrix;
        private int uModelLights;
        private int uInstanceSurface;
        private int uLightColor;
        private int uLightPosition;
        private int uLightIntensity;

        private string name;
        private string shaderLocation; 

        public override string Name { get { return name; } }

        public override string ShaderLocation { get { return shaderLocation; } } // SimpleTexturedMaterial"; } }

        public SimpleTexturedMaterial(bool texture)
        {
            if (texture == true)
            {
                shaderLocation = "~/Shaders/TexturedLitMaterial";
                name = "Simple Textured Material";
            }
            else
            {
                shaderLocation = "~/Shaders/LitMaterial";
                name = "Simple Material";
            }
        }
        /* 
        public void SetupLights(UniformBuffer lightBuffer)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            // Bind the created DiffuseSpecular Buffer to the Buffer Index
            GL.BindBufferRange(BufferRangeTarget.UniformBuffer, uModelLightBufferIndex, lightBuffer.ResourceHandle, (IntPtr)0, (IntPtr)(lightBuffer.ResourceInfo.Stride * lightBuffer.ResourceInfo.Count));

            uModelLights = GL.GetUniformBlockIndex(ProgramHandle, "modelLightBlock");

            GL.UniformBlockBinding(ProgramHandle, uModelLights, (int)uModelLightBufferIndex);
        }
        */ 
        //public void Render(
        //    ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, ref Color4 color, Texture2D modelTexture,
        //    LightingArguments lightingArguments,
        //    Mesh mesh)

        public void Render(
            ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, ref Vector3 color, ref Vector2 surface, Texture2D modelTexture, 
            LightingArguments lightingArguments, ref ModelLightInstance light, 
            VertexBuffer buffer)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            GL.UseProgram(ProgramHandle);

            GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
            GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);

            SetLightingArguments(modelTexture, lightingArguments);

            GL.Uniform3(uInstanceColor, color);
            GL.Uniform2(uInstanceSurface, surface);
            //GL.EnableClientState(ArrayCap.);

            GL.Uniform4(uLightColor, light.Color); // uLightColor = GL.GetUniformLocation(ProgramHandle, "light_color");
            GL.Uniform3(uLightPosition, light.Position); // = GL.GetUniformLocation(ProgramHandle, "light_position");
            GL.Uniform1(uLightIntensity, light.Intensity); // = GL.GetUniformLocation(ProgramHandle, "light_intensity");

            //buffer.Bind(); 
            buffer.Bind();
            MeshVertex.Bind();

            //MeshVertex.Bind();
            GLState.CheckError();

            GL.DrawArrays(PrimitiveType.Triangles, 0, buffer.ResourceInfo.Count);
            GLState.CheckError();

            GL.UseProgram(0);
            GLState.CheckError();

            //buffer.Unbind();
            //GL.BindBuffer(BufferTarget.ArrayBuffer, buffer.ResourceHandle);
            //GLState.CheckError();

            MeshVertex.Unbind();
            //MeshVertex.Unbind();
            GLState.CheckError();
            

            /*
            GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.Vertices.ResourceHandle);
            GLState.CheckError();

            MeshVertex.Bind();
            GLState.CheckError();

            //mesh.Bind();
            //GLState.CheckError();

            //GL.Draw(PrimitiveType.Triangles, indices.ResourceInfo.Count, drawElementsType, IntPtr.Zero);
            //GL.DrawElements(PrimitiveType.Triangles, indices.ResourceInfo.Count, drawElementsType, IntPtr.Zero);
            //Rug.Game.Environment.DrawCallsAccumulator++;
            //GL.DrawArrays(PrimitiveType.Triangles, 0, count);

            GL.DrawArrays(PrimitiveType.Triangles, 0, mesh.Vertices.ResourceInfo.Count);
            GLState.CheckError(); 

            GL.UseProgram(0);
            GLState.CheckError();

            //mesh.Unbind();
            //GLState.CheckError();
            MeshVertex.Unbind();
            GLState.CheckError();
            */
        }


        private void SetLightingArguments(Texture2D modelTexture, LightingArguments lightingArguments)
        {
            int textureIndex = 0;
            
            GLState.BindTexture(TextureUnit.Texture0 + (textureIndex++), modelTexture);
            
            GL.UniformMatrix4(uWorldMatrix, false, ref lightingArguments.WorldMatrix);
            GL.UniformMatrix4(uNormalWorldMatrix, false, ref lightingArguments.NormalWorldMatrix);
            GL.UniformMatrix4(uPerspectiveMatrix, false, ref lightingArguments.ProjectionMatrix);
        }


        public override void LoadResources()
        {
            base.LoadResources();

            GL.UseProgram(ProgramHandle);

            uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
            uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");

            uInstanceColor = GL.GetUniformLocation(ProgramHandle, "instance_color");
            uInstanceSurface = GL.GetUniformLocation(ProgramHandle, "instance_surface");

            uLightColor = GL.GetUniformLocation(ProgramHandle, "light_color");
            uLightPosition = GL.GetUniformLocation(ProgramHandle, "light_position");
            uLightIntensity = GL.GetUniformLocation(ProgramHandle, "light_intensity");

            GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "textureSource"), 0);
            
            //uCameraCenter = GL.GetUniformLocation(ProgramHandle, "cameraCenter");
            uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
            uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");
            uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");

            GL.UseProgram(0);
        }

        protected override void OnLoadResources()
        {
            
        }

        protected override void OnUnloadResources()
        {
            
        }
    }
}
