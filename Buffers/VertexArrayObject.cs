﻿using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace Rug.LiteGL.Buffers
{
    public class VertexArrayObject // : IResource
    {
        private string m_Name;
        private VertexArrayObjectInfo m_ResourceInfo;
        private bool m_IsLoaded;

        private uint m_VAO;
        private uint[] m_BufferHandles;
        private IBuffer[] m_Buffers;

        #region IResource Members

        public string Name
        {
            get { return m_Name; }
        }

        public VertexArrayObjectInfo ResourceInfo
        {
            get { return m_ResourceInfo; }
        }

        public uint ResourceHandle
        {
            get { return m_VAO; }
        }

        public bool IsLoaded
        {
            get { return m_IsLoaded; }
        }

        public bool IsValid
        {
            get;
            private set;
        }

        public VertexArrayObject(string name, IBuffer[] buffers)
        {
            m_Name = name;

            List<IBufferInfo> infos = new List<IBufferInfo>();

            foreach (IBuffer buffer in buffers)
            {
                infos.Add(buffer.ResourceInfo);
            }

            m_Buffers = buffers;

            m_ResourceInfo = new VertexArrayObjectInfo(infos.ToArray());
        }

        public void LoadResources()
        {
            if (m_IsLoaded == true)
            {
                return;
            }

            try
            {
                IsValid = false;
                m_IsLoaded = true;

                // Create the VAO
                GL.GenVertexArrays(1, out m_VAO);

                GLState.CheckError();

                GL.BindVertexArray(m_VAO);

                GLState.CheckError();

                m_BufferHandles = new uint[m_Buffers.Length];

                // Create the buffers for the vertices attributes
                GL.GenBuffers(m_Buffers.Length, m_BufferHandles);

                GLState.CheckError();

                int vertexAttribArray = 0;

                for (int i = 0; i < m_Buffers.Length; i++)
                {
                    m_Buffers[i].LoadResources(m_BufferHandles[i]);

                    //if (m_Buffers[i] is InstanceBuffer)
                    //{
                    //	m_Buffers[i].ResourceInfo.Format.CreateLayout(ref vertexAttribArray, 1); 
                    //}
                    //else 
                    if (m_Buffers[i] is VertexBuffer)
                    {
                        m_Buffers[i].ResourceInfo.Format.CreateLayout(ref vertexAttribArray);
                    }
                    else if (m_Buffers[i] is IndexBuffer)
                    {
                        // do nothing
                    }
                }

                GLState.CheckError();

                // Make sure the VAO is not changed from the outside
                GL.BindVertexArray(0);

                IsValid = true;
            }
            finally
            {

            }
        }

        public void UnloadResources()
        {
            if (m_IsLoaded == false)
            {
                return;
            }

            //Environment.RefrenceCount--;
            //Environment.ResourceNames.Remove(this.Name);

            foreach (IBuffer buffer in m_Buffers)
            {
                buffer.UnloadResources();
            }

            GL.DeleteVertexArrays(1, ref m_VAO);

            //IsValid = false;
            m_IsLoaded = false;
        }

        #endregion

        public void Bind()
        {
            try
            {
                GL.BindVertexArray(m_VAO);

                GLState.CheckError();
            }
            finally
            {
                //IsValid = false;
            }
        }

        public void Unbind()
        {
            try
            {
                GL.BindVertexArray(0);

                GLState.CheckError();
            }
            finally
            {
                //IsValid = false;
            }
        }
    }
}
