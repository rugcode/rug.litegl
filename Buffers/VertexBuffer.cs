﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Rug.LiteGL.Buffers
{
    public class VertexBuffer : IBuffer
	{
		private string m_Name;
		private VertexBufferInfo m_ResourceInfo;
		private bool m_IsLoaded;
		private uint m_Handle;
		private DataStream m_Stream;
		private bool m_IsMapped;
		private bool m_IsValid;

		#region IResource Members

		public string Name
		{
			get { return m_Name; }
		}

		public VertexBufferInfo ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		IBufferInfo IBuffer.ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return m_Handle; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public bool IsValid
		{
			get { return m_IsValid; }			
		}

		public VertexBuffer(string name, VertexBufferInfo resourceInfo)
		{
			m_Name = name;
			m_ResourceInfo = resourceInfo; 
		}

		public void LoadResources()
		{
			if (m_Handle != 0 || m_IsLoaded == true)
			{
				throw new Exception("Attempt to load VertexBuffer resource '" + Name + "', the resource is already loaded");
			}

			GL.GenBuffers(1, out m_Handle);
            GLState.CheckError();

            GL.BindBuffer(BufferTarget.ArrayBuffer, m_Handle);
            GLState.CheckError();

            m_ResourceInfo.OnLoad();

			m_IsLoaded = true; 
		}

		public void LoadResources(uint handle)
		{
			if (m_Handle != 0 || m_IsLoaded == true)
			{
				throw new Exception("Attempt to load VertexBuffer resource '" + Name + "', the resource is already loaded");
			}

			m_Handle = handle;

			GL.BindBuffer(BufferTarget.ArrayBuffer, m_Handle);
            GLState.CheckError();

            m_ResourceInfo.OnLoad();

			m_IsLoaded = true; 
		}


		public void UnloadResources()
		{
			if (m_Handle == 0 || m_IsLoaded == false)
			{
				throw new Exception("Attempt to unload VertexBuffer resource '" + Name + "', the resource is not loaded");
			}

			if (m_IsMapped == true)
			{
				UnmapBuffer(); 
			}

			if (m_Stream != null)
			{
				m_Stream.Dispose();

				m_Stream = null;
			}

			GL.DeleteBuffers(1, ref m_Handle);
			m_Handle = 0; 

			m_IsLoaded = false;
		}

		#endregion

		public void Bind()
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, m_Handle);
        }

		public void Unbind()
		{

		}

		public void MapBuffer(BufferAccess mode, out DataStream stream)
		{
			if (m_IsMapped == true)
			{
				throw new Exception("VertexBuffer resource '" + Name + "' is already mapped");
			}

			GL.BindBuffer(BufferTarget.ArrayBuffer, m_Handle);

			IntPtr ptr = GL.MapBufferRange(BufferTarget.ArrayBuffer, IntPtr.Zero, (IntPtr)(m_ResourceInfo.Count * m_ResourceInfo.Stride), BufferAccessMask.MapInvalidateBufferBit | BufferAccessMask.MapWriteBit);

            GLState.CheckError(); 

			if (m_Stream == null)
			{
				m_Stream = new DataStream(ptr, m_ResourceInfo.Count * m_ResourceInfo.Stride, mode);
			}
			else
			{
				m_Stream.Initiate(ptr, m_ResourceInfo.Count * m_ResourceInfo.Stride, mode); 
			}

			stream = m_Stream;

			m_IsMapped = true;
		}

		public void UnmapBuffer()
		{
			if (m_IsMapped == false)
			{
				throw new Exception("VertexBuffer resource '" + Name + "' is not mapped");
			}

			GL.BindBuffer(BufferTarget.ArrayBuffer, m_Handle);
            
            GLState.CheckError(); 

			m_IsValid = GL.UnmapBuffer(BufferTarget.ArrayBuffer);

            GLState.CheckError(); 

			m_IsMapped = false; 
		}
	}
}
