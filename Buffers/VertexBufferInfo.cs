﻿using System;
using OpenTK.Graphics.OpenGL;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Buffers
{
    public class VertexBufferInfo : IBufferInfo
	{
		private int m_Stride;
		private IVertexFormat m_Format; 
		private int m_Count;
		private BufferUsageHint m_Usage;

		public int Stride
		{
			get { return m_Stride; }
		}

		public IVertexFormat Format
		{
			get { return m_Format; }
		}

		public BufferUsageHint Usage
		{
			get { return m_Usage; }
		}

		public int Count
		{
			get { return m_Count; }
			set { m_Count = value; }
		}

		public VertexBufferInfo(IVertexFormat format, int count, BufferUsageHint usage)
		{
			m_Format = format;
			m_Stride = m_Format.Stride;
			m_Count = count; 
			m_Usage = usage; 
		}		
	
		public void OnLoad()
		{
			GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(m_Count * m_Stride), IntPtr.Zero, m_Usage);
            GLState.CheckError();
        }
	}
}
