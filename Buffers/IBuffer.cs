﻿using OpenTK.Graphics.OpenGL;
namespace Rug.LiteGL.Buffers
{
    public interface IBuffer // : IResource
	{
		IBufferInfo ResourceInfo { get; }

		void Bind();

		void Unbind();

		void MapBuffer(BufferAccess mode, out DataStream stream);

		void UnmapBuffer();

		void LoadResources(uint handle);

        void UnloadResources();
    }
}
