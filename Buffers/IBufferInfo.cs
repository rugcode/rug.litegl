﻿using OpenTK.Graphics.OpenGL;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Buffers
{
    public interface IBufferInfo //: IResourceInfo
	{
		int Stride { get; }

		IVertexFormat Format { get; }

		BufferUsageHint Usage { get; }

		int Count { get; set; }
	}
}
