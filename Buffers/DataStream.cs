﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using OpenTK.Graphics.OpenGL;

namespace Rug.LiteGL.Buffers
{
    public class DataStream : Stream
	{
		private UnmanagedMemoryStream m_UnmanagedStream;
		private bool m_CanRead;
		private bool m_CanWrite;

		public override bool CanSeek
		{
			get { return false; }
		}

		public override bool CanRead
		{
			get { return m_CanRead; }
		}

		public override bool CanWrite
		{
			get { return m_CanWrite; }
		}

		public override void Flush()
		{
			throw new NotImplementedException();
		}

		public override long Length
		{
			get { return m_UnmanagedStream.Length; }
		}

		public override long Position
		{
			get
			{
				return m_UnmanagedStream.Position; 
			}
			set
			{
				m_UnmanagedStream.Position = value; 
			}
		}		

		public DataStream(IntPtr intPtr, int length, BufferAccess mode)
		{
			unsafe 
			{
				m_UnmanagedStream = new UnmanagedMemoryStream((byte*)intPtr.ToPointer(), length); 
			}

			m_CanRead = mode == BufferAccess.ReadWrite || mode == BufferAccess.ReadOnly;
			m_CanWrite = mode == BufferAccess.ReadWrite || mode == BufferAccess.WriteOnly; 
		}

		internal void Initiate(IntPtr intPtr, int length, BufferAccess mode)
		{
			if (m_UnmanagedStream != null) { m_UnmanagedStream.Dispose(); } 

			unsafe 
			{
				m_UnmanagedStream = new UnmanagedMemoryStream((byte*)intPtr.ToPointer(), length);
			}

			m_CanRead = mode == BufferAccess.ReadWrite || mode == BufferAccess.ReadOnly;
			m_CanWrite = mode == BufferAccess.ReadWrite || mode == BufferAccess.WriteOnly; 
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			return m_UnmanagedStream.Read(buffer, offset, count); 
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			m_UnmanagedStream.Write(buffer, offset, count); 
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return m_UnmanagedStream.Seek(offset, origin); 
		}

		public override void SetLength(long value)
		{
			throw new NotImplementedException();
		}
		
		public unsafe void Write<Type>(Type data) where Type : struct
		{
			int size = Marshal.SizeOf(typeof(Type));

			GCHandle data_ptr = GCHandle.Alloc(data, GCHandleType.Pinned);

			IntPtr ptr = data_ptr.AddrOfPinnedObject();

			void* aaPtr = ptr.ToPointer();

			Interop.CustomCopy((void*)m_UnmanagedStream.PositionPointer, aaPtr, size);

			data_ptr.Free();

			m_UnmanagedStream.Position += size;
		}

		public unsafe void WriteRange<Type>(Type[] data) where Type : struct
		{
			int size = Marshal.SizeOf(typeof(Type));

			GCHandle data_ptr = GCHandle.Alloc(data, GCHandleType.Pinned);

			IntPtr ptr = data_ptr.AddrOfPinnedObject();

			void* aaPtr = ptr.ToPointer();

			Interop.CustomCopy((void*)m_UnmanagedStream.PositionPointer, aaPtr, data.Length * size);

			data_ptr.Free();

			m_UnmanagedStream.Position += data.Length * size; 
		}

		public unsafe void WriteRange<Type>(Type[] data, int index, int count) where Type : struct
		{
			int size = Marshal.SizeOf(typeof(Type));

			GCHandle data_ptr = GCHandle.Alloc(data, GCHandleType.Pinned);

			IntPtr ptr = data_ptr.AddrOfPinnedObject();

			byte* aaPtr = (byte*)ptr.ToPointer();

			aaPtr += size * index;
 
			Interop.CustomCopy((void*)m_UnmanagedStream.PositionPointer, (void*)aaPtr, count * size);

			data_ptr.Free();

			m_UnmanagedStream.Position += count * size; 
		}
	}
}
