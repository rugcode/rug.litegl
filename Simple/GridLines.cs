﻿using System;
using OpenTK;
using OpenTK.Graphics;
using Rug.LiteGL.Buffers;
using Rug.LiteGL.Effect;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Simple
{
    public class GridLines
    {
        private object syncLock = new object(); 

        protected GridLineEffect[] Effects;

        private VertexBuffer m_Vertices;

        private bool m_Disposed = true;

        public VertexBuffer Vertices { get { return m_Vertices; } }

        public Vector4[] Data;

        public int DataPoints = 0; 

        public Color4 Color = new Color4(1f, 1f, 0f, 1f);

        public GridLines(SharedEffects effects, int count)
        {
            if (Effects == null)
            {
                Effects = new GridLineEffect[3]; 

                Effects[(int)GridLineEffect.DottedMode.None] = effects.Effects[GridLineEffect.GetName(GridLineEffect.DottedMode.None)] as GridLineEffect;
                Effects[(int)GridLineEffect.DottedMode.Vertical] = effects.Effects[GridLineEffect.GetName(GridLineEffect.DottedMode.Vertical)] as GridLineEffect;
                Effects[(int)GridLineEffect.DottedMode.Horizontal] = effects.Effects[GridLineEffect.GetName(GridLineEffect.DottedMode.Horizontal)] as GridLineEffect;
            }

            Data = new Vector4[count]; 

            m_Vertices = new VertexBuffer("Grid Line Vertices", new VertexBufferInfo(GridLineVertex.Format, count, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));
        }

        public virtual void Render(View3D view, GridLineEffect.DottedMode mode, Vector2 offset, Vector2 scale, Vector4 window)
        {
            if (DataPoints > 0)
            {
                Effects[(int)mode].Render(0, Math.Min(DataPoints, Data.Length), m_Vertices, Color, offset, scale, window);
            }
        }

        public bool Disposed
        {
            get { return m_Disposed; }
        }

        public void LoadResources()
        {
            if (m_Disposed == true)
            {
                m_Vertices.LoadResources();
                m_Disposed = false;
            }
        }

        public void Update()
        {
            if (m_Disposed == true)
            {
                return; 
            }

            lock (syncLock)
            {
                DataStream stream;

                m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

                stream.WriteRange(Data, 0, DataPoints);

                m_Vertices.UnmapBuffer();
            }
        }

        public void UnloadResources()
        {
            if (m_Disposed == false)
            {
                m_Vertices.UnloadResources();

                m_Disposed = true;
            }
        }

        public void Dispose()
        {
            UnloadResources();
        }
    }
}
