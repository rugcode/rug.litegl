﻿using OpenTK;
using OpenTK.Graphics;
using Rug.LiteGL.Buffers;
using Rug.LiteGL.Effect;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Simple
{
    public class TextureQuad
	{
		protected SimpleTexturedQuad[] Effects;

		private Texture2D m_Texture;
		private VertexBuffer m_Vertices;		

		private bool m_Disposed = true;
		private System.Drawing.RectangleF m_CurrentRectangle;
		private System.Drawing.RectangleF m_CurrentTextureRectangle;
		private bool m_CurrentFlipHorizontal = false;
		private bool m_CurrentFlipVertical = false;
		private float m_CurrentDepth = -1f;
		private Color4 m_CurrentColor = new Color4(1f, 1f, 1f, 1f);
        private Color4 m_ColorGradient = new Color4(1f, 1f, 1f, 1f);

        public Texture2D Texture { get { return m_Texture; } set { m_Texture = value; } }
		public VertexBuffer Vertices { get { return m_Vertices; } }

		public System.Drawing.RectangleF Rectangle = new System.Drawing.RectangleF(-1, -1, 2, 2);
        public System.Drawing.RectangleF TextureRectangle = new System.Drawing.RectangleF(0, 0, 1, 1);

		public Color4 Color = new Color4(1f, 1f, 1f, 1f);
        public Color4 ColorGradient = new Color4(1f, 1f, 1f, 1f);

        public float Depth;

        public bool GradientMode = false; 

        public bool FlipHorizontal = false;
		public bool FlipVertical = false;

        public TextureQuad(SharedEffects effects)
		{
			if (Effects == null)
			{
                Effects = new SimpleTexturedQuad[3];

                for (int i = 0; i < 3; i++)
                {
                    Effects[i] = effects.Effects[SimpleTexturedQuad.GetName((BoxMode)i)] as SimpleTexturedQuad;
                }
			}

			m_Vertices = new VertexBuffer("Texture Box Vertices", new VertexBufferInfo(SimpleVertex.Format, 4, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));			
		}

		public virtual void Render(View3D view)
		{
			CheckAndWriteRectangle();

            if (m_Texture == null)
            {
                Effects[(int)BoxMode.Color].Render(OpenTK.Graphics.OpenGL.PrimitiveType.TriangleStrip, 4, m_Vertices);
            }
            else
            {
                Effects[(int)BoxMode.Textured].Render(OpenTK.Graphics.OpenGL.PrimitiveType.TriangleStrip, 4, m_Texture, m_Vertices);
            }
		}

        public virtual void Render(View3D view, Color4 color, bool invert)
        {
            CheckAndWriteRectangle();

            Effects[(int)BoxMode.TexturedColor].Render(OpenTK.Graphics.OpenGL.PrimitiveType.TriangleStrip, 4, m_Texture, color, invert ? new Vector2(-1f, 1f) : new Vector2(1f, 0f), m_Vertices);
        }

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{				
				m_Vertices.LoadResources();

				WriteRectangle(); 		

				m_Disposed = false;
			}
		}

		public void CheckAndWriteRectangle()
		{
			if (FlipHorizontal != m_CurrentFlipHorizontal ||
				FlipVertical != m_CurrentFlipVertical ||
				Rectangle != m_CurrentRectangle ||
				TextureRectangle != m_CurrentTextureRectangle ||
				Depth != m_CurrentDepth ||
				Color != m_CurrentColor ||
                ColorGradient != m_ColorGradient)
			{
				WriteRectangle();
			}
		}

		private void WriteRectangle()
		{
			m_CurrentFlipHorizontal = FlipHorizontal;
			m_CurrentFlipVertical = FlipVertical; 

			m_CurrentRectangle = Rectangle;
            m_CurrentTextureRectangle = TextureRectangle;

			m_CurrentDepth = Depth;

			m_CurrentColor = Color;
            m_ColorGradient = ColorGradient; 

            float minX = m_CurrentRectangle.Left, minY = m_CurrentRectangle.Top, maxX = m_CurrentRectangle.Right, maxY = m_CurrentRectangle.Bottom;
            float minU = m_CurrentTextureRectangle.Left, minV = m_CurrentTextureRectangle.Bottom, maxU = m_CurrentTextureRectangle.Right, maxV = m_CurrentTextureRectangle.Top;

			if (FlipVertical == true)
			{
				float t = minV;
				minV = maxV;
				maxV = t;  
			}

			if (FlipHorizontal == true)
			{
				float t = minU;
				minU = maxU;
				maxU = t;
			}

            Color4 topColor = m_CurrentColor;
            Color4 bottomColor = m_CurrentColor; 

            if (GradientMode == true)
            {
                bottomColor = m_ColorGradient;
            }

            DataStream stream;
			m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

			stream.WriteRange(new SimpleVertex[] {					
						new SimpleVertex() { Position = new Vector3(maxX, minY, m_CurrentDepth), TextureCoords =  new Vector2(maxU, minV), Color = topColor }, 
						new SimpleVertex() { Position = new Vector3(minX, minY, m_CurrentDepth), TextureCoords =  new Vector2(minU, minV), Color = topColor }, 
						new SimpleVertex() { Position = new Vector3(maxX, maxY, m_CurrentDepth), TextureCoords = new Vector2(maxU, maxV), Color = bottomColor },  
						new SimpleVertex() { Position = new Vector3(minX, maxY, m_CurrentDepth), TextureCoords =  new Vector2(minU, maxV), Color = bottomColor } 
					});

			m_Vertices.UnmapBuffer(); 
		}

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_Vertices.UnloadResources();

				m_Disposed = true;
			}
		}

		public void Dispose()
		{
			UnloadResources(); 
		}
	}
}
