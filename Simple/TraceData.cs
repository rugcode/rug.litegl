﻿using System;
using OpenTK;
using OpenTK.Graphics;
using Rug.LiteGL.Buffers;
using Rug.LiteGL.Controls;
using Rug.LiteGL.Effect;
using Rug.LiteGL.Meshes;

namespace Rug.LiteGL.Simple
{
    internal class TraceData
    {
        private object syncLock = new object(); 

        protected GraphLineEffect Effects;

        private VertexBuffer m_Vertices;

        private bool m_Disposed = true;

        private int blockSize = 256; 

        public VertexBuffer Vertices { get { return m_Vertices; } }

        public Vector2[] Data;

        public int DataPoints = 0; 

        public Color4 Color = new Color4(1f, 1f, 0f, 1f);

        public Vector2 LastDataPoint;

        public Vector2 Min;

        public Vector2 Max;

        public bool AssumeLinearXValues = false; 
        public bool RescanXForMinMaxOnBlockUpdate = false;
        public bool RescanYForMinMaxOnBlockUpdate = false;

        public bool RolloutBuffer = true;

        public bool BufferIsFull
        {
            get
            {
                if (RolloutBuffer == true)
                {
                    return DataPoints > Data.Length - blockSize;
                }
                else
                {
                    return DataPoints >= Data.Length; 
                }
            }
        }

        public Vector2 FirstDataPoint
        {
            get
            {
                if (DataPoints == 0)
                {
                    return LastDataPoint; 
                }

                int count = Math.Min(DataPoints, Data.Length);
                int index = 0;

                if (RolloutBuffer == true && count > Data.Length - blockSize)
                {
                    index = count - (Data.Length - blockSize);
                }

                return Data[index];
            }
        }

        public TraceData(SharedEffects effects, int count, int blockSize = 256)
        {
            if (Effects == null)
            {
                Effects = effects.Effects[GraphLineEffect.GetName()] as GraphLineEffect;
            }

            this.blockSize = blockSize; 

            Data = new Vector2[count]; 

            m_Vertices = new VertexBuffer("Line Vertices", new VertexBufferInfo(GraphLineVertex.Format, count, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));
        }

        public virtual void Render(View3D view, Vector2 offset, Vector2 scale, Vector4 window)
        {
            if (DataPoints > 0)
            {
                int count = Math.Min(DataPoints, Data.Length);
                int index = 0;

                if (RolloutBuffer == true && count > Data.Length - blockSize)
                {
                    index = count - (Data.Length - blockSize);
                    count -= index; 
                }

                Effects.Render(index, count, m_Vertices, Color, offset, scale, window);
            }
        }

        public bool Disposed
        {
            get { return m_Disposed; }
        }

        public void LoadResources()
        {
            if (m_Disposed == true)
            {
                m_Vertices.LoadResources();
                m_Disposed = false;
            }
        }

        public void Clear()
        {
            Min = Vector2.Zero;

            Max = Vector2.Zero;

            DataPoints = 0; 
        }

        public void AddDataPoint(Vector2 point)
        {
            point.X = AxesClamp.Clamp(point.X);
            point.Y = AxesClamp.Clamp(point.Y);

            lock (syncLock)
            {
                if (DataPoints >= Data.Length - 1)
                {
                    Array.ConstrainedCopy(Data, blockSize, Data, 0, Data.Length - blockSize);

                    DataPoints = Math.Max(0, DataPoints -= blockSize);

                    if (AssumeLinearXValues == true && RescanXForMinMaxOnBlockUpdate == true)
                    {
                        Min.X = Data[0].X;
                        Max.X = point.X; 
                    }

                    if ((AssumeLinearXValues == false && RescanXForMinMaxOnBlockUpdate == true) || RescanYForMinMaxOnBlockUpdate == true)
                    {
                        Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
                        Vector2 max = new Vector2(float.MinValue, float.MinValue);

                        for (int i = 0; i < DataPoints; i++)
                        {
                            min = Vector2.ComponentMin(min, Data[i]);
                            max = Vector2.ComponentMax(max, Data[i]);
                        }

                        if (RescanXForMinMaxOnBlockUpdate == true)
                        {
                            Min.X = min.X;
                            Max.X = max.X; 
                        }

                        if (RescanYForMinMaxOnBlockUpdate == true)
                        {
                            Min.Y = min.Y;
                            Max.Y = max.Y;
                        }
                    }
                }

                if (DataPoints == 0)
                {
                    Min = point;
                    Max = point; 
                }

                Min = Vector2.ComponentMin(Min, point);
                Max = Vector2.ComponentMax(Max, point);

                Data[DataPoints++] = point;

                LastDataPoint = point; 
            }
        }

        public void Update()
        {
            if (m_Disposed == true)
            {
                return; 
            }

            lock (syncLock)
            {
                DataStream stream;

                m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

                stream.WriteRange(Data, 0, DataPoints);
         
                m_Vertices.UnmapBuffer();
            }
        }

        public void UnloadResources()
        {
            if (m_Disposed == false)
            {
                m_Vertices.UnloadResources();

                m_Disposed = true;
            }
        }

        public void Dispose()
        {
            UnloadResources();
        }
    }
}