﻿using OpenTK;

namespace Rug.LiteGL
{
    public class View2D
    {
        public Viewport Viewport;

        public Vector2 PixelSize;
        public Vector2 ViewportOffset;
        public Vector2 WindowSize;

        public View2D(System.Drawing.Rectangle activeRegion, int windowWidth, int windowHeight)
        {
            Resize2D(activeRegion, windowWidth, windowHeight);
        }

        public virtual void Resize(System.Drawing.Rectangle activeRegion, int windowWidth, int windowHeight)
        {
            Resize2D(activeRegion, windowWidth, windowHeight);
        }

        private void Resize2D(System.Drawing.Rectangle activeRegion, int windowWidth, int windowHeight)
        {
            Viewport = new Viewport(activeRegion.X, activeRegion.Y, activeRegion.Width, activeRegion.Height);
            PixelSize = new Vector2(2f / (activeRegion.Width), 2f / (activeRegion.Height));
            WindowSize = new Vector2((float)windowWidth, (float)windowHeight);

            float leftOffset = (float)activeRegion.Left;
            float rightOffset = (float)(windowWidth - activeRegion.Right);

            float topOffset = (float)activeRegion.Top;
            float bottomOffset = (float)(windowHeight - activeRegion.Bottom);

            ViewportOffset = new Vector2((rightOffset - leftOffset) * 0.5f, (bottomOffset - topOffset) * 0.5f);
        }
    }

    public enum ProjectionKind
    {
        Perspective,
        Orthographic
    }

    public class View3D : View2D
    {
        //public Camera Camera;
        ///public ViewFrustum Frustum;

        public Matrix4 View = Matrix4.Identity;
        public Matrix4 ViewCenter = Matrix4.Identity;
        public Matrix4 Projection = Matrix4.Identity;
        public Matrix4 ProjectionInverse = Matrix4.Identity;
        public Matrix4 World = Matrix4.Identity;
        public Matrix4 NormalWorld = Matrix4.Identity;
        public Matrix4 WorldInverse = Matrix4.Identity;

        public Vector3 LightDirection;
        public Vector2 RangeZ = new Vector2(0.1f, 6000f);
        public Vector3 OrthographicBoxSize = new Vector3(100, 100, 200);

        public bool UpdateFrustum = true;

        public Quaternion Rotation = Quaternion.Identity;
        public Vector3 Center = Vector3.Zero;
        public float Offset = -10f;

        public float FOV;
        private float AspectRatio;

        public ProjectionKind ProjectionKind { get; set; }

        public View3D(System.Drawing.Rectangle activeRegion, int windowWidth, int windowHeight, float FOV)
            : base(activeRegion, windowWidth, windowHeight)
        {
            ProjectionKind = ProjectionKind.Perspective;
            this.FOV = FOV;
            AspectRatio = (float)activeRegion.Width / (float)activeRegion.Height;
            
            //Camera = new Camera(FOV, (float)activeRegion.Width / (float)activeRegion.Height, Scale);
            //Frustum = new ViewFrustum();

            UpdateProjection();

            SetGlobalLight(new Vector3(0, 0, 0), new Vector3(0, 1f, 1f));
        }

        public override void Resize(System.Drawing.Rectangle activeRegion, int windowWidth, int windowHeight)
        {
            base.Resize(activeRegion, windowWidth, windowHeight);

            AspectRatio = (float)activeRegion.Width / (float)activeRegion.Height;

            //Camera.Setup(Camera.FOV, (float)activeRegion.Width / (float)activeRegion.Height, Camera.Scale);

            UpdateProjection();
        }

        public void SetGlobalLight(Vector3 pos, Vector3 lookAt)
        {
            LightDirection = lookAt - pos;
            LightDirection.Normalize();
        }

        public void LookAt(Vector3 from, Vector3 target)
        {
            Vector3 dir = from - target;
            dir.Normalize();

            Vector3 right = new Vector3(dir.Z, 0, -dir.X);
            right.Normalize();

            Vector3 up = Vector3.Cross(dir, right);
        }

        public virtual void UpdateProjection()
        {
            if (ProjectionKind == ProjectionKind.Perspective)
            {
                Projection = Matrix4.CreatePerspectiveFieldOfView(FOV, AspectRatio, RangeZ.X, RangeZ.Y);
            }
            else
            {
                Projection = Matrix4.CreateOrthographicOffCenter(
                    -OrthographicBoxSize.X, OrthographicBoxSize.X,
                    -OrthographicBoxSize.Y, OrthographicBoxSize.Y,
                    -OrthographicBoxSize.Z, OrthographicBoxSize.Z);
            }

            ProjectionInverse = Matrix4.Invert(Projection);

            Matrix4 matRot = Matrix4.CreateFromQuaternion(Quaternion.Conjugate(Rotation));

            ViewCenter = (Matrix4.CreateTranslation(Center) * matRot);

            Vector3 fwd = Vector3.TransformVector(Vector3.UnitZ, matRot);

            float offset = Offset;

            View =
                (Matrix4.CreateFromQuaternion(Quaternion.Conjugate(Rotation))) *
                Matrix4.CreateTranslation(Vector3.UnitZ * offset) *
                (Matrix4.CreateTranslation(Center))
                ;

            World = View;
            //WorldInverse = Matrix4.Invert(View);

            Quaternion normalQuaternion = Rotation;
            normalQuaternion.Invert();

            NormalWorld = Matrix4.CreateFromQuaternion(normalQuaternion);
            //NormalWorld = WorldInverse;
            //NormalWorld.Transpose();

            /*
            if (Camera.IsFreeCamera)
            {
                Matrix4 matRot = Matrix4.CreateFromQuaternion(Quaternion.Conjugate(Camera.Rotation));

                ViewCenter = (Matrix4.CreateTranslation(Camera.Center) * matRot);

                Vector3 fwd = Vector3.TransformVector(Vector3.UnitZ, matRot);

                float offset = Camera.Offset;
                if (Camera.ViewRotation != 0)
                {
                    offset *= -1f;
                }

                View =
                    Matrix4.CreateTranslation(Camera.Forward * offset) *
                    (Matrix4.CreateTranslation(Camera.Center) *
                    (Matrix4.CreateFromQuaternion(Quaternion.Conjugate(Camera.Rotation)) *
                     Matrix4.CreateRotationY(Camera.ViewRotation) *
                     Matrix4.CreateRotationX(Camera.ViewElevation)));

                World = View;
                //WorldInverse = Matrix4.Invert(View);

                Quaternion normalQuaternion = Camera.Rotation;
                normalQuaternion.Invert();

                NormalWorld = Matrix4.CreateFromQuaternion(normalQuaternion);
                //NormalWorld = WorldInverse;
                //NormalWorld.Transpose();
            }
            else
            {

            }

            if (UpdateFrustum == true)
            {
                Frustum.Setup(this, Camera);
            }
            */
        }

        public Vector2 TransformMouseCoords(System.Windows.Forms.Form form, System.Windows.Forms.MouseEventArgs e)
        {
            if ((form == null) ||
                (form.ClientSize.Width == Viewport.Width && form.ClientSize.Height == Viewport.Height))
            {
                return new Vector2(e.X, e.Y);
            }

            return new Vector2(e.X, e.Y);
        }

        public Vector2 TransformMouseCoords(System.Windows.Forms.Form form, System.Drawing.Point point)
        {
            if ((form == null) ||
                (form.ClientSize.Width == Viewport.Width && form.ClientSize.Height == Viewport.Height))
            {
                return new Vector2(point.X, point.Y);
            }

            return new Vector2(point.X, point.Y);
        }

        public Vector2 TransformMouseRay(System.Windows.Forms.Form form, System.Windows.Forms.MouseEventArgs e)
        {
            Vector2 coords = TransformMouseCoords(form, e);

            return new Vector2((coords.X / (float)Viewport.Width) * 2.0f - 1.0f, ((coords.Y / (float)Viewport.Height) * 2.0f - 1.0f) * -1.0f);
        }

        public Vector2 TransformMouseRay(Vector2 coords)
        {
            return new Vector2((coords.X / (float)Viewport.Width) * 2.0f - 1.0f, ((coords.Y / (float)Viewport.Height) * 2.0f - 1.0f) * -1.0f);
        }

        public bool NeedsResize(System.Drawing.Size size)
        {
            return Viewport.Width != size.Width || Viewport.Height != size.Height;
        }
    }
}