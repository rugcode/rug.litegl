﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

namespace Rug.LiteGL
{
    internal static partial class Helper
    {
        public static void AppendAttributeAndValue(XmlElement element, string name, System.Drawing.Color value) { Helper.AppendAttributeAndValue(element, name, SerializeColor(value)); }

        public static System.Drawing.Color GetAttributeValue(XmlNode node, string name, System.Drawing.Color @default)
        {
            if (node.Attributes[name] == null)
            {
                return @default;
            }

            try
            {
                return DeserializeColor(node.Attributes[name].Value);
            }
            catch
            {
                return @default;
            }
        }

        public static string SerializeColor(System.Drawing.Color color)
        {
            return Color.ToARGBHtmlString(color);
        }

        public static System.Drawing.Color DeserializeColor(string color)
        {
            return Color.FromHtmlString(color, false);
        }

        #region Color

        public static class Color
        {
            private readonly static List<string> Colors = new List<string>(Enum.GetNames(typeof(System.Drawing.KnownColor)));

            public static string ToHtmlString(System.Drawing.Color color)
            {
                return "#" + color.R.ToString("X2", CultureInfo.InvariantCulture) + color.G.ToString("X2", CultureInfo.InvariantCulture) + color.B.ToString("X2", CultureInfo.InvariantCulture);
            }

            public static string ToARGBHtmlString(System.Drawing.Color color)
            {
                return "#" + color.A.ToString("X2", CultureInfo.InvariantCulture) + color.R.ToString("X2", CultureInfo.InvariantCulture) + color.G.ToString("X2", CultureInfo.InvariantCulture) + color.B.ToString("X2", CultureInfo.InvariantCulture);
            }

            public static System.Drawing.Color FromHtmlString(string color)
            {
                return FromHtmlString(color, true);
            }

            public static System.Drawing.Color FromHtmlString(string color, bool throwException)
            {
                string str = color;

                if (Helper.IsNullOrEmpty(str))
                {
                    throw new ArgumentNullException("color");
                }

                str = str.Trim();

                if (str.StartsWith("#"))
                {
                    str = str.Substring(1);
                }

                if (str.Length == 8)
                {
                    byte a, r, g, b;

                    if ((byte.TryParse(str.Substring(0, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out a)) &&
                        (byte.TryParse(str.Substring(2, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out r)) &&
                        (byte.TryParse(str.Substring(4, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out g)) &&
                        (byte.TryParse(str.Substring(6, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out b)))
                        return System.Drawing.Color.FromArgb(a, r, g, b);
                }

                if (str.Length == 6)
                {
                    byte r, g, b;

                    if ((byte.TryParse(str.Substring(0, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out r)) &&
                        (byte.TryParse(str.Substring(2, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out g)) &&
                        (byte.TryParse(str.Substring(4, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out b)))
                        return System.Drawing.Color.FromArgb(r, g, b);
                }

                if (str.Length == 3)
                {
                    byte r, g, b;

                    if ((byte.TryParse(str.Substring(0, 1), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out r)) &&
                        (byte.TryParse(str.Substring(1, 1), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out g)) &&
                        (byte.TryParse(str.Substring(2, 1), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out b)))
                        return System.Drawing.Color.FromArgb(r * 16, g * 16, b * 16);
                }

                if (Colors.Contains(str))
                {
                    return System.Drawing.Color.FromName(str);
                }

                if (throwException)
                {
                    throw new Exception("Color is of unknown format '" + color + "'");
                }
                else
                {
                    return System.Drawing.Color.Transparent;
                }
            }
        }

        #endregion
    }
}
