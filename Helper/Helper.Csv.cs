﻿using System;
using System.Globalization;
using System.Text;

namespace Rug.LiteGL
{
    static partial class Helper
    {
        public static string ToCsv(params object[] cells)
        {
            StringBuilder sb = new StringBuilder();

            string seperatorString = new string(',', 1);

            foreach (object obj in cells)
            {
                if (obj is bool)
                {
                    sb.Append(((bool)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is int)
                {
                    sb.Append(((int)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is uint)
                {
                    sb.Append(((uint)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is long)
                {
                    sb.Append(((long)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is ulong)
                {
                    sb.Append(((ulong)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is short)
                {
                    sb.Append(((short)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is ushort)
                {
                    sb.Append(((ushort)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is float)
                {
                    sb.Append(((float)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is double)
                {
                    sb.Append(((double)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is decimal)
                {
                    sb.Append(((decimal)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else if (obj is TimeSpan)
                {
                    sb.Append(((TimeSpan)obj).TotalMinutes.ToString(CultureInfo.InvariantCulture) + seperatorString);
                }
                else
                {
                    sb.Append(obj.ToString() + seperatorString);
                }
            }

            string final = sb.ToString();

            // strip of the final separator string 
            final = final.Substring(0, final.Length - seperatorString.Length);

            return final;
        }
    }
}
