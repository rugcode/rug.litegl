﻿using System;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Rug.LiteGL.Meshes
{
    public interface IVertexFormat
    {
        int Stride { get; }

        void CreateLayout(ref int baseLocation);

        void CreateLayout(ref int baseLocation, int devisor);
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MeshVertex
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TextureCoords;
        public Color4 Color;

        public enum Elements : int { Position = 0, Normal = 1, TextureCoords = 2, Color = 3, };

        public static readonly int Stride;
        public static readonly int PositionOffset;
        public static readonly int NormalOffset;
        public static readonly int TextureCoordsOffset;
        public static readonly int ColorOffset;

        static MeshVertex()
        {
            Stride = BlittableValueType<MeshVertex>.Stride;

            PositionOffset = (int)Marshal.OffsetOf(typeof(MeshVertex), "Position");
            NormalOffset = (int)Marshal.OffsetOf(typeof(MeshVertex), "Normal");
            TextureCoordsOffset = (int)Marshal.OffsetOf(typeof(MeshVertex), "TextureCoords");
            ColorOffset = (int)Marshal.OffsetOf(typeof(MeshVertex), "Color");

            if (ColorOffset + BlittableValueType<Color4>.Stride != Stride)
            {
                throw new Exception("Stride does not match offset total");
            }
        }

        public static void Bind()
        {
            GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
            GL.VertexAttribPointer((int)Elements.Normal, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
            GL.VertexAttribPointer((int)Elements.TextureCoords, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
            GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

            GL.EnableVertexAttribArray((int)Elements.Position);
            GL.EnableVertexAttribArray((int)Elements.Normal);
            GL.EnableVertexAttribArray((int)Elements.TextureCoords);
            GL.EnableVertexAttribArray((int)Elements.Color);
        }

        public static void Unbind()
        {
            GL.DisableVertexAttribArray((int)Elements.Position);
            GL.DisableVertexAttribArray((int)Elements.Normal);
            GL.DisableVertexAttribArray((int)Elements.TextureCoords);
            GL.DisableVertexAttribArray((int)Elements.Color);
        }


        public readonly static IVertexFormat Format = new FormatInfo();

        #region Format Class

        private class FormatInfo : IVertexFormat
        {
            #region IVertexFormat Members

            public int Stride
            {
                get { return MeshVertex.Stride; }
            }

            public void CreateLayout(ref int baseLocation)
            {
                GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
                GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
                GL.VertexAttribPointer((int)Elements.TextureCoords + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
                GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

                GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.TextureCoords + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

                baseLocation += 4;
            }

            public void CreateLayout(ref int baseLocation, int devisor)
            {
                GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
                GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
                GL.VertexAttribPointer((int)Elements.TextureCoords + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
                GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

                GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.TextureCoords + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

                GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
                GL.Arb.VertexAttribDivisor((int)Elements.Normal + baseLocation, devisor);
                GL.Arb.VertexAttribDivisor((int)Elements.TextureCoords + baseLocation, devisor);
                GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);

                baseLocation += 4;
            }

            #endregion
        }

        #endregion
    }
}
