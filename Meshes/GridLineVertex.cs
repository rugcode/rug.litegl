﻿using System;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Rug.LiteGL.Meshes
{
    [StructLayout(LayoutKind.Sequential)]
    public struct GridLineVertex
    {
        public Vector4 Position;

        public enum Elements : int { Position = 0 };
        public static readonly int Stride;
        public static readonly int PositionOffset;

        static GridLineVertex()
        {
            Stride = BlittableValueType<GridLineVertex>.Stride;

            PositionOffset = (int)Marshal.OffsetOf(typeof(GridLineVertex), "Position");

            if (PositionOffset + BlittableValueType<Vector4>.Stride != Stride)
            {
                throw new Exception("Stride does not match offset total");
            }
        }

        public static void Bind()
        {
            GL.VertexAttribPointer((int)Elements.Position, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);

            GL.EnableVertexAttribArray((int)Elements.Position);
        }

        public static void Unbind()
        {
            GL.DisableVertexAttribArray((int)Elements.Position);
        }

        public readonly static IVertexFormat Format = new FormatInfo();

        #region Format Class

        private class FormatInfo : IVertexFormat
        {
            #region IVertexFormat Members

            public int Stride
            {
                get { return GraphLineVertex.Stride; }
            }

            public void CreateLayout(ref int baseLocation)
            {
                GL.VertexAttribPointer((int)Elements.Position + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);

                GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);

                baseLocation += 1;
            }

            public void CreateLayout(ref int baseLocation, int devisor)
            {
                GL.VertexAttribPointer((int)Elements.Position + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);

                GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);

                GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);

                baseLocation += 1;
            }

            #endregion
        }

        #endregion
    }
}