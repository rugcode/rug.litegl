﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using OpenTK;
using OpenTK.Graphics;

namespace Rug.LiteGL.Meshes
{
    public static class WavefrontObjLoader
    {
        public static void NormaliseAndCenterise(params MeshVertex[][] modelVerts)
        {
            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            foreach (MeshVertex[] verts in modelVerts)
            {
                for (int i = 0; i < verts.Length; i++)
                {
                    min = Vector3.ComponentMin(min, verts[i].Position);
                    max = Vector3.ComponentMax(max, verts[i].Position);
                }
            }

            Vector3 size = (max - min); 
            Vector3 center = size * 0.5f + min;

            size.X = Math.Abs(size.X);
            size.Y = Math.Abs(size.Y);
            size.Z = Math.Abs(size.Z);

            float maxComponent = Math.Max(Math.Max(size.X, size.Y), size.Z);

            float scale = 1f / (maxComponent * 0.5f);

            foreach (MeshVertex[] verts in modelVerts)
            {
                for (int i = 0; i < verts.Length; i++)
                {
                    Vector3 position = verts[i].Position;

                    position -= center;

                    position *= scale;

                    verts[i].Position = position;
                }
            }
        }

        public static void FlattenIndices(string path, out MeshVertex[] modelVerts, bool normaliseAndCenterise = true)
        {
            MeshVertex[] verts;
            ushort[] indices;

            LoadObject(path, out verts, out indices);

            modelVerts = new MeshVertex[indices.Length];

            int i = 0;
            for (int j = 0; j < indices.Length; j += 3)
            {
                MeshVertex vert1 = verts[j + 0];
                MeshVertex vert2 = verts[j + 2];
                MeshVertex vert3 = verts[j + 1];

                modelVerts[i++] = new MeshVertex()
                {
                    Position = vert1.Position,
                    Normal = vert1.Normal,
                    Color = vert1.Color,
                    TextureCoords = vert1.TextureCoords,
                };

                modelVerts[i++] = new MeshVertex()
                {
                    Position = vert2.Position,
                    Normal = vert2.Normal,
                    Color = vert2.Color,
                    TextureCoords = vert2.TextureCoords,
                };

                modelVerts[i++] = new MeshVertex()
                {
                    Position = vert3.Position,
                    Normal = vert3.Normal,
                    Color = vert3.Color,
                    TextureCoords = vert3.TextureCoords,
                };
            }

            if (normaliseAndCenterise == true)
            {
                NormaliseAndCenterise(modelVerts); 
            }
        }

        public static void FlattenIndices(string path, out SimpleVertex[] modelVerts)
        {
            MeshVertex[] verts;
            ushort[] indices;

            LoadObject(path, out verts, out indices);

            modelVerts = new SimpleVertex[indices.Length];

            int i = 0;
            for (int j = 0; j < indices.Length; j += 3)
            {
                MeshVertex vert1 = verts[j + 0];
                MeshVertex vert2 = verts[j + 2];
                MeshVertex vert3 = verts[j + 1];

                modelVerts[i++] = new SimpleVertex()
                {
                    Position = vert1.Position,
                    Color = vert1.Color,
                    TextureCoords = vert1.TextureCoords,
                };

                modelVerts[i++] = new SimpleVertex()
                {
                    Position = vert2.Position,
                    Color = vert2.Color,
                    TextureCoords = vert2.TextureCoords,
                };

                modelVerts[i++] = new SimpleVertex()
                {
                    Position = vert3.Position,
                    Color = vert3.Color,
                    TextureCoords = vert3.TextureCoords,
                };
            }
        }
        

        public static void LoadObject(string path, out MeshVertex[] verts, out ushort[] indicies)
        {
            IEnumerable<string> lines = FileHelper.ReadAllLines(path);

            LoadObject(lines, out verts, out indicies);
        }

        public static void LoadObject(byte[] data, out MeshVertex[] verts, out ushort[] indicies)
        {
            using (MemoryStream stream = new MemoryStream(data))
            using (StreamReader reader = new StreamReader(stream))
            {
                string[] lines = reader.ReadToEnd().Split(new string[] { "\n", System.Environment.NewLine }, StringSplitOptions.None);

                LoadObject(lines, out verts, out indicies);
            }
        }

        public static void LoadObject(IEnumerable<string> lines, out MeshVertex[] verts, out ushort[] indicies)
        {
            List<Vector3> v = new List<Vector3>();
            List<Vector3> n = new List<Vector3>();
            List<Vector2> t = new List<Vector2>();

            List<string> faces = new List<string>();

            foreach (string line in lines)
            {
                if (line.StartsWith("#") == true)
                {
                    continue;
                }

                if (String.IsNullOrEmpty(line.Trim()) == true)
                {
                    continue;
                }

                if (line.StartsWith("v ") == true)
                {
                    v.Add(ParseVector3(line.Substring(2)));
                }
                else if (line.StartsWith("vn ") == true)
                {
                    n.Add(ParseVector3(line.Substring(3)));
                }
                else if (line.StartsWith("vt ") == true)
                {
                    t.Add(ParseVector2(line.Substring(3)));
                }
                else if (line.StartsWith("f ") == true)
                {
                    faces.Add(line.Substring(2));
                }
            }

            if (v.Count == 0)
            {
                v.Add(new Vector3(0));
            }

            if (n.Count == 0)
            {
                n.Add(Vector3.UnitY);
            }

            if (t.Count == 0)
            {
                t.Add(new Vector2(0));
            }

            List<MeshVertex> vertBuilder = new List<MeshVertex>();
            List<ushort> i = new List<ushort>();
            Color4 color = new Color4(1f, 1f, 1f, 1f);
            ushort index = 0;

            foreach (string face in faces)
            {
                string[] faceParts = face.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (faceParts.Length == 3)
                {
                    int vi0, ni0, ti0;
                    ParseFacePart(faceParts[0], out vi0, out ti0, out ni0);

                    int vi1, ni1, ti1;
                    ParseFacePart(faceParts[1], out vi1, out ti1, out ni1);

                    int vi2, ni2, ti2;
                    ParseFacePart(faceParts[2], out vi2, out ti2, out ni2);

                    MeshVertex v1 = new MeshVertex()
                    {
                        Position = v[vi0 - 1],
                        Normal = n[ni0 - 1],
                        TextureCoords = t[ti0 - 1],
                        Color = color,
                    };

                    MeshVertex v2 = new MeshVertex()
                    {
                        Position = v[vi1 - 1],
                        Normal = n[ni1 - 1],
                        TextureCoords = t[ti1 - 1],
                        Color = color,
                    };

                    MeshVertex v3 = new MeshVertex()
                    {
                        Position = v[vi2 - 1],
                        Normal = n[ni2 - 1],
                        TextureCoords = t[ti2 - 1],
                        Color = color,
                    };

                    vertBuilder.Add(v1);
                    vertBuilder.Add(v2);
                    vertBuilder.Add(v3);
                    i.Add(index++);
                    i.Add(index++);
                    i.Add(index++);
                }
                else if (faceParts.Length == 4)
                {
                    int vi0, ni0, ti0;
                    ParseFacePart(faceParts[0], out vi0, out ti0, out ni0);

                    int vi1, ni1, ti1;
                    ParseFacePart(faceParts[1], out vi1, out ti1, out ni1);

                    int vi2, ni2, ti2;
                    ParseFacePart(faceParts[2], out vi2, out ti2, out ni2);

                    int vi3, ni3, ti3;
                    ParseFacePart(faceParts[3], out vi3, out ti3, out ni3);

                    MeshVertex v1 = new MeshVertex()
                    {
                        Position = v[vi0 - 1],
                        Normal = n[ni0 - 1],
                        TextureCoords = t[ti0 - 1],
                        Color = color,
                    };

                    MeshVertex v2 = new MeshVertex()
                    {
                        Position = v[vi1 - 1],
                        Normal = n[ni1 - 1],
                        TextureCoords = t[ti1 - 1],
                        Color = color,
                    };

                    MeshVertex v3 = new MeshVertex()
                    {
                        Position = v[vi2 - 1],
                        Normal = n[ni2 - 1],
                        TextureCoords = t[ti2 - 1],
                        Color = color,
                    };

                    MeshVertex v4 = new MeshVertex()
                    {
                        Position = v[vi3 - 1],
                        Normal = n[ni3 - 1],
                        TextureCoords = t[ti3 - 1],
                        Color = color,
                    };

                    ushort baseIndex = index;

                    vertBuilder.Add(v1);
                    vertBuilder.Add(v2);
                    vertBuilder.Add(v3);
                    vertBuilder.Add(v4);

                    i.Add((ushort)(baseIndex + 0));
                    i.Add((ushort)(baseIndex + 1));
                    i.Add((ushort)(baseIndex + 2));

                    i.Add((ushort)(baseIndex + 2));
                    i.Add((ushort)(baseIndex + 3));
                    i.Add((ushort)(baseIndex + 0));

                    index += 4;
                }
            }

            verts = vertBuilder.ToArray();
            indicies = i.ToArray();
        }

        public static void LoadObject_Indexed(string path, Vector4 material, out Vector3[] verts, out ushort[] indicies)
        {
            IEnumerable<string> lines = FileHelper.ReadAllLines(path);

            LoadObject_Indexed(lines, material, out verts, out indicies);
        }

        public static void LoadObject_Indexed(byte[] data, Vector4 material, out Vector3[] verts, out ushort[] indicies)
        {
            using (MemoryStream stream = new MemoryStream(data))
            using (StreamReader reader = new StreamReader(stream))
            {
                string[] lines = reader.ReadToEnd().Split(new string[] { "\n", System.Environment.NewLine }, StringSplitOptions.None);

                LoadObject_Indexed(lines, material, out verts, out indicies);
            }
        }

        public static void LoadObject_Indexed(IEnumerable<string> lines, Vector4 material, out Vector3[] verts, out ushort[] indicies)
        {
            List<Vector3> v = new List<Vector3>();
            List<Vector3> n = new List<Vector3>();
            List<Vector2> t = new List<Vector2>();

            List<string> faces = new List<string>();

            foreach (string line in lines)
            {
                if (line.StartsWith("#") == true)
                {
                    continue;
                }

                if (String.IsNullOrEmpty(line.Trim()) == true)
                {
                    continue;
                }

                if (line.StartsWith("v ") == true)
                {
                    v.Add(ParseVector3(line.Substring(2)));
                }
                else if (line.StartsWith("vn ") == true)
                {
                    n.Add(ParseVector3(line.Substring(3)));
                }
                else if (line.StartsWith("vt ") == true)
                {
                    t.Add(ParseVector2(line.Substring(3)));
                }
                else if (line.StartsWith("f ") == true)
                {
                    faces.Add(line.Substring(2));
                }
            }

            List<Vector3> vertBuilder = new List<Vector3>(v.ToArray());
            List<ushort> i = new List<ushort>();
            Color4 color = new Color4(1f, 1f, 1f, 0f);
            ushort index = 0;

            foreach (string face in faces)
            {
                string[] faceParts = face.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (faceParts.Length == 3)
                {
                    int vi0, ni0, ti0;
                    ParseFacePart(faceParts[0], out vi0, out ti0, out ni0);

                    int vi1, ni1, ti1;
                    ParseFacePart(faceParts[1], out vi1, out ti1, out ni1);

                    int vi2, ni2, ti2;
                    ParseFacePart(faceParts[2], out vi2, out ti2, out ni2);

                    i.Add((ushort)(vi0 - 1));
                    i.Add((ushort)(vi1 - 1));
                    i.Add((ushort)(vi2 - 1));
                }
                else if (faceParts.Length == 4)
                {
                    int vi0, ni0, ti0;
                    ParseFacePart(faceParts[0], out vi0, out ti0, out ni0);

                    int vi1, ni1, ti1;
                    ParseFacePart(faceParts[1], out vi1, out ti1, out ni1);

                    int vi2, ni2, ti2;
                    ParseFacePart(faceParts[2], out vi2, out ti2, out ni2);

                    int vi3, ni3, ti3;
                    ParseFacePart(faceParts[3], out vi3, out ti3, out ni3);

                    ushort baseIndex = index;

                    i.Add((ushort)(vi0 - 1));
                    i.Add((ushort)(vi1 - 1));
                    i.Add((ushort)(vi2 - 1));

                    i.Add((ushort)(vi2 - 1));
                    i.Add((ushort)(vi3 - 1));
                    i.Add((ushort)(vi0 - 1));
                }
            }

            verts = vertBuilder.ToArray();
            indicies = i.ToArray();
        }

        private static Vector2 ParseVector2(string str)
        {
            string[] parts = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            return new Vector2(float.Parse(parts[0], CultureInfo.InvariantCulture),
                                float.Parse(parts[1], CultureInfo.InvariantCulture));
        }

        private static Vector3 ParseVector3(string str)
        {
            string[] parts = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            return new Vector3(float.Parse(parts[0], CultureInfo.InvariantCulture),
                               float.Parse(parts[1], CultureInfo.InvariantCulture),
                               float.Parse(parts[2], CultureInfo.InvariantCulture));
        }

        private static void ParseFacePart(string part, out int vi, out int ti, out int ni)
        {
            string[] parts = part.Split(new char[] { '/' }, StringSplitOptions.None);

            if (int.TryParse(parts[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out vi) == false)
            {
                vi = 1; 
            }

            if (int.TryParse(parts[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out ti) == false)
            {
                ti = 1;
            }

            if (int.TryParse(parts[2], NumberStyles.Integer, CultureInfo.InvariantCulture, out ni) == false)
            {
                ni = 1;
            }
        }
    }
}
