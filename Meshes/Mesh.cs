﻿using OpenTK.Graphics.OpenGL;
using Rug.LiteGL.Buffers;

namespace Rug.LiteGL.Meshes
{
    public class Mesh // : IResource
    {
        private bool m_IsLoaded = false;

        private string m_Name;

        private VertexBuffer m_Vertices;

        private MeshVertex[] m_VertsData;

        public float FloatID { get; set; }

        public bool IsVisible = true;

        public string Name
        {
            get { return m_Name; }
        }
        
        public uint ResourceHandle
        {
            get { return 0; }
        }

        public bool IsLoaded
        {
            get { return m_IsLoaded; }
        }

        public VertexBuffer Vertices
        {
            get { return m_Vertices; }
        }

        public VertexArrayObject VAO { get; private set; }

        public Mesh(string name, MeshVertex[] verts)
        {
            m_Name = name;

            m_VertsData = verts;

            m_Vertices = new VertexBuffer(m_Name + " Vertices", new VertexBufferInfo(MeshVertex.Format, verts.Length, BufferUsageHint.StaticDraw));

            //VAO = new VertexArrayObject(Name + " VAO", new IBuffer[] { m_Vertices });
        }

        public void LoadResources()
        {
            if (m_IsLoaded == false)
            {
                m_Vertices.LoadResources();

                //m_Indices.LoadResources();

                //VAO.LoadResources(); 

                UploadData();

                m_IsLoaded = true;
            }
        }

        public void UploadData()
        {
            DataStream stream;
            m_Vertices.MapBuffer(BufferAccess.WriteOnly, out stream);
            stream.WriteRange(m_VertsData);
            m_Vertices.UnmapBuffer();
        }

        public void UnloadResources()
        {
            if (m_IsLoaded == true)
            {
                //VAO.UnloadResources();
                m_Vertices.UnloadResources();
                m_IsLoaded = false;
            }
        }

        public void Bind()
        {
            //VAO.Bind(); 
            m_Vertices.Bind();
            MeshVertex.Bind();
        }

        public void Unbind()
        {
            //VAO.Unbind();
            MeshVertex.Unbind();
        }
    }
}
