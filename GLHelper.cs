﻿using System;
using System.Diagnostics;
using OpenTK.Graphics.OpenGL;

namespace Rug.LiteGL
{
    public static class GLHelper
    {
        //[Conditional("DEBUG")]
        public static void CheckFramebufferExt()
        {
            switch (GL.Ext.CheckFramebufferStatus(FramebufferTarget.FramebufferExt))
            {
                case FramebufferErrorCode.FramebufferCompleteExt:
                    break;
                case FramebufferErrorCode.FramebufferIncompleteAttachmentExt:
                    throw new Exception("OpenGL error: Frame buffer object one or more attachment points are not framebuffer attachment complete. This could mean there’s no texture attached or the format isn’t renderable. For color textures this means the base format must be RGB or RGBA and for depth textures it must be a DEPTH_COMPONENT format. Other causes of this error are that the width or height is zero or the z-offset is out of range in case of render to volume.");
                case FramebufferErrorCode.FramebufferIncompleteMissingAttachmentExt:
                    throw new Exception("OpenGL error: Frame buffer object there are no attachments.");
                case FramebufferErrorCode.FramebufferIncompleteDimensionsExt:
                    throw new Exception("OpenGL error: Frame buffer object attachments are of different size. All attachments must have the same width and height.");
                case FramebufferErrorCode.FramebufferIncompleteFormatsExt:
                    throw new Exception("OpenGL error: Frame buffer object the color attachments have different format. All color attachments must have the same format.");
                case FramebufferErrorCode.FramebufferIncompleteDrawBufferExt:
                    throw new Exception("OpenGL error: Frame buffer object an attachment point referenced by GL.DrawBuffers() doesn’t have an attachment.");
                case FramebufferErrorCode.FramebufferIncompleteReadBufferExt:
                    throw new Exception("OpenGL error: Frame buffer object the attachment point referenced by GL.ReadBuffers() doesn’t have an attachment.");
                case FramebufferErrorCode.FramebufferUnsupportedExt:
                    throw new Exception("OpenGL error: Frame buffer object this particular FBO configuration is not supported by the implementation.");
                default:
                    throw new Exception("OpenGL error: Frame buffer object status unknown. (yes, this is really bad.)");


            }
        }

        [Conditional("DEBUG")]
        internal static void CheckTexture()
        {
            switch (GL.GetError())
            {
                case ErrorCode.NoError:
                    return;
                case ErrorCode.InvalidEnum:
                    throw new Exception("OpenGL error: Invalid enum.");
                case ErrorCode.InvalidOperation:
                    throw new Exception("OpenGL error: Invalid operation.");
                case ErrorCode.InvalidValue:
                    throw new Exception("OpenGL error: Invalid value.");
                case ErrorCode.OutOfMemory:
                    throw new Exception("OpenGL error: Out Of memory.");
                case ErrorCode.StackOverflow:
                    throw new Exception("OpenGL error: Stack overflow.");
                case ErrorCode.StackUnderflow:
                    throw new Exception("OpenGL error: Stack underflow.");
                case ErrorCode.TableTooLarge:
                    throw new Exception("OpenGL error: Table too large.");
                case ErrorCode.TextureTooLargeExt:
                    throw new Exception("OpenGL error: Texture too large.");
                default:
                    break;
            }
        }

        public static int MultiSampleToSampleCount(MultiSamples multisamples)
        {
            switch (multisamples)
            {
                case MultiSamples.X1:
                    return 1;
                case MultiSamples.X2:
                    return 2;
                case MultiSamples.X4:
                    return 4;
                case MultiSamples.X8:
                    return 8;
                case MultiSamples.X16:
                    return 16;
                default:
                    return 1;
            }
        }

        public static MultiSamples SampleCountToMultiSample(int value)
        {
            switch (value)
            {
                case 1:
                    return MultiSamples.X1;
                case 2:
                    return MultiSamples.X2;
                case 4:
                    return MultiSamples.X4;
                case 8:
                    return MultiSamples.X8;
                case 16:
                    return MultiSamples.X16;
                default:
                    return MultiSamples.X1;
            }
        }
    }
}
