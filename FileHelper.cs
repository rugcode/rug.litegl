﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Rug.LiteGL
{
    public static class FileHelper
    {
        public static bool Exists(string path)
        {
            return FileExists(path);
        }

        public static bool FileExists(string path)
        {
            string resolvedPath = Helper.ResolvePath(path);

            return File.Exists(resolvedPath);
        }

        public static DateTime GetLastWriteTime(string path)
        {
            if (FileExists(path) == true)
            {
                return File.GetLastWriteTime(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find file at path '{0}'", path));
            }
        }

        public static void LoadXml(System.Xml.XmlDocument doc, string path)
        {
            if (FileExists(path) == true)
            {
                doc.Load(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find XML file at path '{0}'", path));
            }
        }

        public static IEnumerable<string> ReadAllLines(string path)
        {
            if (FileExists(path) == true)
            {
                return File.ReadAllLines(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find text file at path '{0}'", path));
            }
        }

        public static byte[] ReadAllBytes(string path)
        {
            if (FileExists(path) == true)
            {
                return File.ReadAllBytes(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find binary file at path '{0}'", path));
            }
        }

        public static Bitmap LoadBitmap(string path)
        {
            if (FileExists(path) == true)
            {
                return (Bitmap)Image.FromFile(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find Bitmap file at path '{0}'", path));
            }
        }
    }
}
